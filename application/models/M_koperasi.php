<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_koperasi extends CI_Model{

	// login
    function proseslogin($user,$pass){
        $this->db->where('u_name',$user);
        $this->db->where('pass_word',$pass);
        return $this->db->get('tbl_user')->row();
	}

	// Jenis Pinjaman
	function read_jns_pinjaman(){
		return $this->db->get('jns_simpan');
	}
	function edit_jns_pinjaman($id){
		$this->db->where("id",$id);
		return $this->db->get('jns_simpan');
	}
	function update_jns_pinjaman($id,$data){
		$this->db->where("id",$id);
		$this->db->update('jns_simpan',$data);
	}
	
	// Jenis Akun
	function read_jns_akun(){
		return $this->db->get('jns_akun');
	}
	function edit_jns_akun($id){
		$this->db->where("id",$id);
		return $this->db->get('jns_akun');
	}
	function update_jns_akun($id,$data){
		$this->db->where("id",$id);
		$this->db->update('jns_akun',$data);
	}
	function create_jns_akun($data){
        $this->db->insert('jns_akun',$data);
    }
	
	// Data Kas
	function read_data_kas(){
		return $this->db->get('nama_kas_tbl');
	}
	function edit_data_kas($id){
		$this->db->where("id",$id);
		return $this->db->get('nama_kas_tbl');
	}
	function update_data_kas($id,$data){
		$this->db->where("id",$id);
		$this->db->update('nama_kas_tbl',$data);
	}
	function create_data_kas($data){
		$this->db->insert('nama_kas_tbl',$data);
    }
	function delete_data_kas($id){
		$this->db->where("id", $id);
		$this->db->delete('nama_kas_tbl', array('id' => $id));
	}
	
	// Lama Angsuran
	function read_lama_angsuran(){
		return $this->db->query('SELECT * FROM jns_angsuran ORDER BY ket ASC');
	}
	function edit_lama_angsuran($id){
		$this->db->where("id",$id);
		return $this->db->get('jns_angsuran');
	}
	function update_lama_angsuran($id,$data){
		$this->db->where("id",$id);
		$this->db->update('jns_angsuran',$data);
	}
	function create_lama_angsuran($data){
		$this->db->insert('jns_angsuran',$data);
    }
	function delete_lama_angsuran($id){
		$this->db->where("id", $id);
		$this->db->delete('jns_angsuran', array('id' => $id));
	}
	
	// Data Barang
	function read_data_barang(){
		return $this->db->get('tbl_barang');
	}
	function edit_data_barang($id){
		$this->db->where("id",$id);
		return $this->db->get('tbl_barang');
	}
	function update_data_barang($id,$data){
		$this->db->where("id",$id);
		$this->db->update('tbl_barang',$data);
	}
	function create_data_barang($data){
		$this->db->insert('tbl_barang',$data);
    }
	function delete_data_barang($id){
		$this->db->where("id", $id);
		$this->db->delete('tbl_barang', array('id' => $id));
	}

	// Data Anggota
	function read_data_anggota(){
		return $this->db->get('tbl_anggota');
	}
	function edit_data_anggota($id){
		$this->db->where("id",$id);
		return $this->db->get('tbl_anggota');
	}
	function update_data_anggota($id,$data){
		$this->db->where("id",$id);
		$this->db->update('tbl_anggota',$data);
	}
	function create_data_anggota($data){
		$this->db->insert('tbl_anggota',$data);
    }
	function delete_data_anggota($id){
		$this->db->where("id", $id);
		$this->db->delete('tbl_anggota', array('id' => $id));
	}
	
	// Data Pengguna
	function read_data_pengguna(){
		return $this->db->get('tbl_user');
	}
	function edit_data_pengguna($id){
		$this->db->where("id",$id);
		return $this->db->get('tbl_user');
	}
	function update_data_pengguna($id,$data){
		$this->db->where("id",$id);
		$this->db->update('tbl_user',$data);
	}
	function create_data_pengguna($data){
		$this->db->insert('tbl_user',$data);
    }
	function delete_data_pengguna($id){
		$this->db->where("id", $id);
		$this->db->delete('tbl_user', array('id' => $id));
	}
	
	// Identitas Koperasi
	function read_identitas_koperasi(){
		return $this->db->get('tbl_setting');
	}
	function edit_identitas_koperasi($id){
		$this->db->where("id",$id);
		return $this->db->get('tbl_setting');
	}
	function update_identitas_koperasi($id,$data){
		$this->db->where("id",$id);
		$this->db->update('tbl_setting',$data);
	}

	// Suku Bunga
	function read_suku_bunga(){
		return $this->db->get('suku_bunga');
	}
	function edit_suku_bunga($id){
		$this->db->where("id",$id);
		return $this->db->get('suku_bunga');
	}
	function update_suku_bunga($id,$data){
		$this->db->where("id",$id);
		$this->db->update('suku_bunga',$data);
	}
	
	// Kas Pemasukan
	function read_transaksi_kas_pemasukan1(){
		$query = $this->db->query('SELECT t.id as id_kas, t.tgl_catat as tgl, t.keterangan as ket, a.jns_trans as dari_akun, s.nama as untuk_kas, t.jumlah as jml, t.user_name as user FROM tbl_trans_kas t 
										JOIN tbl_user u ON(u.u_name = t.user_name )
										JOIN nama_kas_tbl s ON(s.id = t.untuk_kas_id)
										JOIN jns_akun a ON(a.id = t.jns_trans)
										WHERE t.akun = "Pemasukan" ');
		return $query;
	}
	function get_tbl_jns_akun(){
		$query = $this->db->query('SELECT * FROM jns_akun ');
		return $query;
	}
	function get_tbl_kas(){
		$query = $this->db->query('SELECT * FROM nama_kas_tbl ');
		return $query;
	}
	function edit_transaksi_kas_pemasukan($id){
		$this->db->where("id",$id);
		return $this->db->get('tbl_trans_kas');
	}
	function update_transaksi_kas_pemasukan($id,$data){
		$this->db->where("id",$id);
		$this->db->update('tbl_trans_kas',$data);
	}
	function create_transaksi_kas_pemasukan($data){
		$this->db->insert('tbl_trans_kas',$data);
    }
	function delete_transaksi_kas_pemasukan($id){
		$this->db->where("id", $id);
		$this->db->delete('tbl_trans_kas', array('id' => $id));
	}
	
	// Kas Pengeluaran
	function read_transaksi_kas_pengeluaran1(){
		$query = $this->db->query('SELECT t.id as id_kas, t.tgl_catat as tgl, t.keterangan as ket, a.jns_trans as untuk_akun, s.nama as dari_kas, t.jumlah as jml, t.user_name as user FROM tbl_trans_kas t 
										JOIN tbl_user u ON(u.u_name = t.user_name )
										JOIN nama_kas_tbl s ON(s.id = t.dari_kas_id)
										JOIN jns_akun a ON(a.id = t.jns_trans)
										WHERE t.akun = "Pengeluaran" ');
		return $query;
	}
	function edit_transaksi_kas_pengeluaran($id){
		$this->db->where("id",$id);
		return $this->db->get('tbl_trans_kas');
	}
	function update_transaksi_kas_pengeluaran($id,$data){
		$this->db->where("id",$id);
		$this->db->update('tbl_trans_kas',$data);
	}
	function create_transaksi_kas_pengeluaran($data){
		$this->db->insert('tbl_trans_kas',$data);
    }
	function delete_transaksi_kas_pengeluaran($id){
		$this->db->where("id", $id);
		$this->db->delete('tbl_trans_kas', array('id' => $id));
	}
	
	// Kas Transkfer
	function read_transaksi_kas_transfer1(){
		$query = $this->db->query('SELECT t.id as id_kas, t.tgl_catat as tgl, t.keterangan as ket, s.nama as dari_kas, n.nama as untuk_kas, t.jumlah as jml, t.user_name as user FROM tbl_trans_kas t 
										JOIN tbl_user u ON(u.u_name = t.user_name )
										JOIN nama_kas_tbl s ON(s.id = t.dari_kas_id)
										JOIN nama_kas_tbl n ON(n.id = t.untuk_kas_id)
										WHERE t.akun = "Transfer" ');
		return $query;
	}
	function edit_transaksi_kas_transfer($id){
		$this->db->where("id",$id);
		return $this->db->get('tbl_trans_kas');
	}
	function update_transaksi_kas_transfer($id,$data){
		$this->db->where("id",$id);
		$this->db->update('tbl_trans_kas',$data);
	}
	function create_transaksi_kas_transfer($data){
		$this->db->insert('tbl_trans_kas',$data);
    }
	function delete_transaksi_kas_transfer($id){
		$this->db->where("id", $id);
		$this->db->delete('tbl_trans_kas', array('id' => $id));
	}
	
	// Setoran Tunai
	function read_setoran_tunai(){
		$query = $this->db->query('SELECT s.id as id_simpan, s.tgl_transaksi as tgl, a.id as id_anggota, a.nama as nama_anggota, a.departement as departement, j.jns_simpan as jns_simpan, s.jumlah as jml, s.user_name as user FROM tbl_trans_sp s 
										JOIN tbl_anggota a ON(a.id = s.anggota_id )
										JOIN nama_kas_tbl k ON(k.id = s.kas_id )
										JOIN jns_simpan j ON(j.id = s.jenis_id )
										JOIN tbl_user u ON(u.u_name = s.user_name )
										WHERE dk = "D" ');
		return $query;
	}
	function get_tbl_anggota(){
		$query = $this->db->query('SELECT * FROM tbl_anggota');
		return $query;
	}
	function get_tbl_jns_simpanan(){
		$query = $this->db->query('SELECT * FROM jns_simpan ');
		return $query;
	}
	function edit_setoran_tunai($id){
		$this->db->where("id",$id);
		return $this->db->get('tbl_trans_sp');
	}
	function update_setoran_tunai($id,$data){
		$this->db->where("id",$id);
		$this->db->update('tbl_trans_sp',$data);
	}
	function create_setoran_tunai($data){
		$this->db->insert('tbl_trans_sp',$data);
    }
	function delete_setoran_tunai($id){
		$this->db->where("id", $id);
		$this->db->delete('tbl_trans_sp', array('id' => $id));
	}
	
	// Penarikan Tunai
	function read_penarikan_tunai(){
		$query = $this->db->query('SELECT s.id as id_simpan, s.tgl_transaksi as tgl, a.id as id_anggota, a.nama as nama_anggota, a.departement as departement, j.jns_simpan as jns_simpan, s.jumlah as jml, s.user_name as user FROM tbl_trans_sp s 
										JOIN tbl_anggota a ON(a.id = s.anggota_id )
										JOIN nama_kas_tbl k ON(k.id = s.kas_id )
										JOIN jns_simpan j ON(j.id = s.jenis_id )
										JOIN tbl_user u ON(u.u_name = s.user_name )
										WHERE dk = "K" ');
		return $query;
	}
	function edit_penarikan_tunai($id){
		$this->db->where("id",$id);
		return $this->db->get('tbl_trans_sp');
	}
	function update_penarikan_tunai($id,$data){
		$this->db->where("id",$id);
		$this->db->update('tbl_trans_sp',$data);
	}
	function create_penarikan_tunai($data){
		$this->db->insert('tbl_trans_sp',$data);
    }
	function delete_penarikan_tunai($id){
		$this->db->where("id", $id);
		$this->db->delete('tbl_trans_sp', array('id' => $id));
	}
	
	// Data Pengajuan
	function read_data_pengajuan(){
		$query = $this->db->query('SELECT a.id AS id, a.no_ajuan AS no_ajuan, a.ajuan_id AS ajuan_id, a.anggota_id AS anggota_id, 
									a.tgl_input AS tgl_input, a.jenis AS jenis, a.nominal AS nominal, a.lama_ags AS lama_ags, a.keterangan AS keterangan, 
									a.status AS status, a.alasan AS alasan, a.tgl_update AS tgl_update, a.tgl_cair AS tgl_cair,
									b.identitas AS identitas, b.nama AS nama, b.departement AS departement
									FROM tbl_pengajuan AS a
									LEFT JOIN tbl_anggota AS b ON b.id = a.anggota_id ');
		return $query;
	}
	
	// Data Pinjaman
	function read_data_pinjaman(){
		$query = $this->db->query('SELECT v.id as id, v.tgl_pinjam as tgl_pinjam, a.identitas as identitas, a.nama as nama,
									a.departement as departement, b.nm_barang as nm_barang, v.jumlah as jumlah, v.lama_angsuran as lama_angsuran,
									v.pokok_angsuran as pokok_angsuran, v.bunga_pinjaman as bunga_pinjaman, v.biaya_adm as biaya_adm,
									v.ags_per_bulan as ags_per_bulan, v.lunas as lunas, v.user_name as username 
									FROM v_hitung_pinjaman v
									JOIN tbl_anggota a ON (a.id = v.anggota_id)
									JOIN tbl_barang b ON (b.id = v.barang_id)
									WHERE dk = "K"  ');
		return $query;
	}
	function read_data_barang1(){
		$this->db->where("jml_brg !=", '0');
		return $this->db->get('tbl_barang');
	}
	function edit_data_pinjaman($id){
		$this->db->where("id",$id);
		return $this->db->get('tbl_pinjaman_h');
	}
	function update_data_pinjaman($id,$data){
		$this->db->where("id",$id);
		$this->db->update('tbl_pinjaman_h',$data);
	}
	function create_data_pinjaman($data){
		$this->db->trans_start();
		// update stok barang berkurang
		$this->db->where('id', $this->input->post('barang_id'));
		$this->db->where('type <>', 'Uang');
		$this->db->set('jml_brg', 'jml_brg - 1', FALSE);
		$this->db->update('tbl_barang');

		$this->db->insert('tbl_pinjaman_h',$data);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
    }
	function delete_data_pinjaman($id){
		$this->db->trans_start();

		// update stok barang bertambah
		$this->db->select('barang_id');
		$this->db->from('tbl_pinjaman_h');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$row = $query->row();
		$barang_id = $row->barang_id;
		$this->db->where('id', $barang_id);
		$this->db->set('jml_brg', 'jml_brg + 1', FALSE);
		$this->db->update('tbl_barang');

		$this->db->where("id", $id);
		$this->db->delete('tbl_pinjaman_h', array('id' => $id));

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return FALSE;
		} else {
			$this->db->trans_complete();
			return TRUE;
		}
	}

	function detail_data_pinjaman($id){
		// $this->db->where("v.id",$id);
		// $query = $this->db->query('SELECT v.id as id, v.tgl_pinjam as tgl_pinjam, a.identitas as identitas, a.nama as nama, a.file_pic as file_pic,
		// 							a.departement as departement, a.tmp_lahir as tmp_lahir, a.tgl_lahir as tgl_lahir, a.kota as kota, 
		// 							b.nm_barang as nm_barang, v.jumlah as jumlah, v.lama_angsuran as lama_angsuran, v.tgl_pinjam as tgl_pinjam,
		// 							v.pokok_angsuran as pokok_angsuran, v.bunga_pinjaman as bunga_pinjaman, v.biaya_adm as biaya_adm, v.tempo as tempo,
		// 							v.ags_per_bulan as ags_per_bulan, v.lunas as lunas, v.user_name as username 
		// 							FROM v_hitung_pinjaman v
		// 							JOIN tbl_anggota a ON (a.id = v.anggota_id)
		// 							JOIN tbl_barang b ON (b.id = v.barang_id)');
		// return $query;
	$this->db->select('v.id as id, v.tgl_pinjam as tgl_pinjam, a.identitas as identitas, a.nama as nama, a.id as id_anggota, a.file_pic as file_pic,
						a.departement as departement, a.tmp_lahir as tmp_lahir, a.tgl_lahir as tgl_lahir, a.kota as kota, 
						b.nm_barang as nm_barang, v.jumlah as jumlah, v.lama_angsuran as lama_angsuran, v.tgl_pinjam as tgl_pinjam,
						v.pokok_angsuran as pokok_angsuran, v.bunga_pinjaman as bunga_pinjaman, v.biaya_adm as biaya_adm, v.tempo as tempo,
						v.ags_per_bulan as ags_per_bulan, v.lunas as lunas, v.user_name as username ');
	$this->db->from('v_hitung_pinjaman v');
	$this->db->join('tbl_anggota a' , 'a.id = v.anggota_id');
	$this->db->join('tbl_barang b' , 'b.id = v.barang_id');
	$this->db->where('v.id',$id);
	$query = $this->db->get();
	return $query;
	}

	function detail_data_angsuran($id){
	$this->db->select('d.id as id, d.tgl_bayar as tgl_bayar, d.angsuran_ke as angsuran_ke, d.ket_bayar as angsuran,
					   d.jumlah_bayar as jumlah_bayar, SUM(d.jumlah_bayar) as total_jml_bayar, SUM(d.denda_rp) as total_denda, d.denda_rp as denda, d.user_name as username');
	$this->db->from('tbl_pinjaman_d d');
	$this->db->join('v_hitung_pinjaman v', 'v.id = d.pinjam_id');
	$this->db->where('d.pinjam_id',$id);
	$query = $this->db->get();
	return $query;
}

	//Pelunasan Pinjaman
	function read_pelunasan_pinjaman(){
		$this->db->select('v.id as id, a.nama as nama_anggota, a.identitas as identitas, a.departement as departement,
							v.tgl_pinjam as tgl_pinjam, v.tempo as tempo, v.lama_angsuran as lama_angsuran, v.tagihan as total_tagihan, 
							sum(d.denda_rp) as total_denda, sum(d.jumlah_bayar) as total_bayar');
		$this->db->from('v_hitung_pinjaman v');
		$this->db->join('tbl_pinjaman_d d', 'v.id = d.pinjam_id');
		$this->db->join('tbl_anggota a', 'a.id = v.anggota_id');
		$this->db->where('v.lunas =', 'Lunas');
		$query = $this->db->get();
		return $query;
		}

	//Laporan Data Kas Anggota
	function total_simpanan($id){
		$this->db->select(' sum(d.jumlah) as jml_setor');
		$this->db->from('tbl_anggota a');
		$this->db->join('tbl_trans_sp d', 'a.id = d.anggota_id');
		$this->db->where('d.anggota_id = ', $id);
		$query = $this->db->get();
		return $query;
	}
	function read_jatuh_tempo(){
		$this->db->select('v_hitung_pinjaman.id AS id,v_hitung_pinjaman.tempo AS tempo,v_hitung_pinjaman.tgl_pinjam AS tgl_pinjam, v_hitung_pinjaman.tagihan AS tagihan,
		 v_hitung_pinjaman.lama_angsuran AS lama_angsuran, tbl_anggota.identitas AS identitas, tbl_anggota.nama AS nama, SUM(tbl_pinjaman_d.jumlah_bayar) AS jum_bayar,
		  SUM(tbl_pinjaman_d.denda_rp) AS jum_denda ');
		$this->db->from('v_hitung_pinjaman');
		$this->db->where('lunas','Belum');
		
		$this->db->join('tbl_anggota', 'tbl_anggota.id = v_hitung_pinjaman.anggota_id', 'LEFT');
		$this->db->join('tbl_pinjaman_d', 'tbl_pinjaman_d.pinjam_id = v_hitung_pinjaman.id', 'LEFT');
		$this->db->order_by('v_hitung_pinjaman.tempo', 'ASC');
		$this->db->group_by('v_hitung_pinjaman.id');
		$query = $this->db->get();
		return $query;
	}
	function read_kredit_macet(){
		$this->db->select('v_hitung_pinjaman.id AS id,v_hitung_pinjaman.tempo AS tempo,v_hitung_pinjaman.tgl_pinjam AS tgl_pinjam, v_hitung_pinjaman.tagihan AS tagihan, 
		v_hitung_pinjaman.lama_angsuran AS lama_angsuran, tbl_anggota.nama AS nama, SUM(tbl_pinjaman_d.jumlah_bayar) AS jum_bayar, SUM(tbl_pinjaman_d.denda_rp) AS jum_denda');
		$this->db->from('v_hitung_pinjaman');
		$this->db->where('lunas','Belum');
		
		$this->db->join('tbl_anggota', 'tbl_anggota.id = v_hitung_pinjaman.anggota_id', 'LEFT');
		$this->db->join('tbl_pinjaman_d', 'tbl_pinjaman_d.pinjam_id = v_hitung_pinjaman.id', 'LEFT');
		$this->db->order_by('v_hitung_pinjaman.tempo', 'ASC');
		$this->db->group_by('v_hitung_pinjaman.id');
		$query = $this->db->get();
		return $query;
	}
	function read_transaksi_kas(){
		$this->db->select('*');
		$this->db->from('v_transaksi v');
		
		$query = $this->db->get();
		return $query;
	}
	function read_neraca_saldo(){
		$query = $this->db->query("SELECT * FROM jns_akun WHERE aktif = 'Y' ORDER BY kd_aktiva ASC");
		
		return $query;
	}
	function read_kas_simpanan(){
		$this->db->select('*');
		$this->db->from('jns_simpan');
		$this->db->where('tampil','Y');
		$query = $this->db->get();
		return $query;
	}
	// Kas Pinjaman
	function read_pinjaman_aktif(){
		$this->db->select('*');
		$this->db->from('v_hitung_pinjaman');
		$query = $this->db->get();
		return $query->num_rows();
	}
	function read_pinjaman_lunas(){
		$this->db->select('*');
		$this->db->from('v_hitung_pinjaman');
		$this->db->where('lunas','Lunas');
		$query = $this->db->get();
		return $query->num_rows();
	}
	function read_pinjaman_blm(){
		$this->db->select('*');
		$this->db->from('v_hitung_pinjaman');
		$this->db->where('lunas','Belum');
		$query = $this->db->get();
		return $query->num_rows();
	}
	function read_jml_pinjaman() {
		$this->db->select('SUM(jumlah) AS jml_total');
		$this->db->from('v_hitung_pinjaman');

		$query = $this->db->get();
		return $query->row();
	}
	function read_jml_tagihan() {
		$this->db->select('SUM(tagihan) AS jml_total');
		$this->db->from('v_hitung_pinjaman');

		$query = $this->db->get();
		return $query->row();
	}
	function read_jml_denda() {
		$this->db->select('SUM(denda_rp) AS total_denda');
		$this->db->from('tbl_pinjaman_d');
		$this->db->join('tbl_pinjaman_h', 'tbl_pinjaman_h.id = tbl_pinjaman_d.pinjam_id', 'LEFT');

		$query = $this->db->get();
		return $query->row();
	}
	function read_jml_angsuran() {
		$this->db->select('SUM(jumlah_bayar) AS jml_total');
		$this->db->from('tbl_pinjaman_d');
		$this->db->join('tbl_pinjaman_h', 'tbl_pinjaman_h.id = tbl_pinjaman_d.pinjam_id', 'LEFT');

		$query = $this->db->get();
		return $query->row();
	}
	//Data Kas
	function read_jml_biaya_adm() {
		$this->db->select('SUM(biaya_adm * lama_angsuran) AS jml_total');
		$this->db->from('v_hitung_pinjaman');

		$query = $this->db->get();
		return $query->row();
	}
	function read_jml_bunga() {
		$this->db->select('SUM(bunga_pinjaman * lama_angsuran) AS jml_total');
		$this->db->from('v_hitung_pinjaman');

		$query = $this->db->get();
		return $query->row();
	}
	// Get data anggota
	function get_data_anggota($id) {
		$this->db->select('*');
		$this->db->from('tbl_anggota');
		$this->db->where('aktif', 'Y');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query;
		// $out = array();
		// $sql = "SELECT * FROM tbl_anggota WHERE aktif='Y'";
		// $sql .=" AND (id = '".$id."') ";
		// $query = $this->db->query($sql);
		// if($query->num_rows() > 0) {
		// 	$out = $query->row();
		// 	return $out;
		}
	function get_simpanan_wajib($id){
		$this->db->select('SUM(jumlah) as jml_simpanan');
		$this->db->from('tbl_trans_sp');
		$this->db->where('anggota_id', $id);
		$this->db->where('jenis_id =', '41');
		$query = $this->db->get();
		return $query;
	}
	function get_simpanan_pokok($id){
		$this->db->select('SUM(jumlah) as jml_simpanan');
		$this->db->from('tbl_trans_sp');
		$this->db->where('anggota_id', $id);
		$this->db->where('jenis_id =', '40');
		$query = $this->db->get();
		return $query;
	}
	function get_simpanan_sukarela($id){
		$this->db->select('SUM(jumlah) as jml_simpanan');
		$this->db->from('tbl_trans_sp');
		$this->db->where('anggota_id', $id);
		$this->db->where('jenis_id =', '32');
		$query = $this->db->get();
		return $query;

	}
	function get_pinjaman($id){
		$this->db->select('SUM(jumlah) as jml_pinjaman');
		$this->db->from('v_hitung_pinjaman');
		$this->db->where('anggota_id', $id);
		$query = $this->db->get();
		return $query;
	}
	function get_pengajuan($id){
		$this->db->select('*');
		$this->db->from('tbl_pengajuan');
		$this->db->where('anggota_id',$id);
		$query = $this->db->get();
		return $query;
	}
	function create_data_pengajuan($data){
		$this->db->insert('tbl_pengajuan',$data);
	}
	// Beranda
	function create_beranda($data){
        $this->db->insert('config_beranda',$data);
    }
	function read_beranda(){
        return $this->db->query("SELECT * FROM config_beranda");
	}
	function edit_beranda($id){
        $this->db->where("id_cb",$id);
        return $this->db->get('config_beranda');
    }
	function update_beranda($id,$data){
        $this->db->where("id_cb",$id);
        $this->db->update('config_beranda',$data);
	}
	function delete_beranda($id){
		$this->db->where("id_cb", $id);
        $query = $this->db->get('config_beranda');
        $row = $query->row();
        unlink("upload/config_beranda/$row->background");
        $this->db->delete('config_beranda', array('id_cb' => $id));
	}
	
	// Tentang
	function create_tentang($data){
        $this->db->insert('config_tentang',$data);
	}
	function read_tentang(){
        return $this->db->query("SELECT * FROM config_tentang");
	}
	function edit_tentang($id){
        $this->db->where("id_ct",$id);
        return $this->db->get('config_tentang');
	}
	function update_tentang($id,$data){
        $this->db->where("id_ct",$id);
        $this->db->update('config_tentang',$data);
	}
	function delete_tentang($id){
        $this->db->where("id_ct", $id);
        $query = $this->db->get('config_tentang');
        $row = $query->row();
        unlink("upload/config_tentang/$row->cover_halaman");
        unlink("upload/config_tentang/$row->foto_perusahaan");
        $this->db->delete('config_tentang', array('id_ct' => $id));
	}

	// Kontak
	function create_kontak($data){
        $this->db->insert('config_kontak',$data);
	}
	function read_kontak(){
        return $this->db->query("SELECT * FROM config_kontak");
	}
	function edit_kontak($id){
        $this->db->where("id_ck",$id);
        return $this->db->get('config_kontak');
	}
	function update_kontak($id,$data){
        $this->db->where("id_ck",$id);
        $this->db->update('config_kontak',$data);
	}
	function delete_kontak($id){
        $this->db->where("id_ck", $id);
        $query = $this->db->get('config_kontak');
        $row = $query->row();
        unlink("upload/config_kontak/$row->cover_halaman");
        $this->db->delete('config_kontak', array('id_ck' => $id));
	}

	// Produk
	function create_produk($data){
        $this->db->insert('produk',$data);
	}
	function read_produk(){
        return $this->db->query("SELECT * FROM produk");
	}
	function edit_produk($id){
        $this->db->where("id_produk",$id);
        return $this->db->get('produk');
	}
	function update_produk($id,$data){
        $this->db->where("id_produk",$id);
        $this->db->update('produk',$data);
	}
	function delete_produk($id){
        $this->db->where("id_produk", $id);
        $query = $this->db->get('produk');
        $row = $query->row();
        unlink("upload/produk/$row->foto_produk");
        $this->db->delete('produk', array('id_produk' => $id));
	}

	// Kelas
	function create_kelas($data){
        $this->db->insert('kelas',$data);
	}
	function read_kelas(){
        return $this->db->query("SELECT * FROM kelas");
	}
	function edit_kelas($id){
        $this->db->where("id_kelas",$id);
        return $this->db->get('kelas');
	}
	function update_kelas($id,$data){
        $this->db->where("id_kelas",$id);
        $this->db->update('kelas',$data);
	}
	function delete_kelas($id){
        $this->db->where("id_kelas", $id);
        $this->db->delete('kelas', array('id_kelas' => $id));
	}

	// Kategori
	function create_kategori($data){
        $this->db->insert('kategori',$data);
	}
	function read_kategori(){
        return $this->db->query("SELECT * FROM kategori");
	}
	function edit_kategori($id){
        $this->db->where("id_kategori",$id);
        return $this->db->get('kategori');
	}
	function update_kategori($id,$data){
        $this->db->where("id_kategori",$id);
        $this->db->update('kategori',$data);
	}
	function delete_kategori($id){
        $this->db->where("id_kategori", $id);
        $this->db->delete('kategori', array('id_kategori' => $id));
	}
	// Pekerjaan
	function read_tbl_pekerjaan(){
		return $this->db->get('pekerjaan');
	}
}
