<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	// Controller Data Anggota
	public function data_anggota()	{
		$data['data'] = $this->m_koperasi->read_data_anggota()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Laporan/v_data_anggota',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	// Controller Data Kas Anggota
	public function kas_anggota()	{
		$data['data'] = $this->m_koperasi->read_data_anggota()->result();
		// $data['data1'] = $this->m_koperasi->total_simpanan($id)->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Laporan/v_kas_anggota',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	// Controller Data Jatuh Tempo
	public function jatuh_tempo()	{
		$data['data'] = $this->m_koperasi->read_jatuh_tempo()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Laporan/v_jatuh_tempo',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	// Controller Data Kredit Macet
	public function kredit_macet()	{
		$data['data'] = $this->m_koperasi->read_kredit_macet()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Laporan/v_kredit_macet',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	// Controller Data Transaksi Kas
	public function transaksi_kas()	{
		$data['data'] = $this->m_koperasi->read_transaksi_kas()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Laporan/v_transaksi_kas',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	// Controller Buku Besar
	public function buku_besar()	{
		$data['data'] = $this->m_koperasi->read_transaksi_kas()->result();
		// $data['data1'] = $this->m_koperasi->total_simpanan($id)->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Laporan/v_buku_besar',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	// Controller Neraca Saldo
	public function neraca_saldo()	{
		$data['data'] = $this->m_koperasi->read_neraca_saldo()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Laporan/v_neraca_saldo',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	// Controller Kas Simmpanan
	public function kas_simpanan()	{
		$data['data'] = $this->m_koperasi->read_kas_simpanan()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Laporan/v_kas_simpanan',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	// Controller Kas pinjaman
	public function kas_pinjaman()	{
		$data['data'] = $this->m_koperasi->read_pinjaman_aktif();
		$data['data1'] = $this->m_koperasi->read_pinjaman_lunas();
		$data['data2'] = $this->m_koperasi->read_pinjaman_blm();
		$data['data3'] = $this->m_koperasi->read_jml_pinjaman();
		$data['data4'] = $this->m_koperasi->read_jml_tagihan();
		$data['data5'] = $this->m_koperasi->read_jml_denda();
		$data['data6'] = $this->m_koperasi->read_jml_angsuran();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Laporan/v_kas_pinjaman',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	// Controller Kas Simmpanan
	public function saldo_kas()	{
		$data['data'] = $this->m_koperasi->read_data_kas()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
		$data['pages'] = $this->load->view('pages/Laporan/v_saldo_kas',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	// Controller Kas Simmpanan
	public function laba_rugi()	{
		$data['data'] = $this->m_koperasi->read_jml_pinjaman();
		$data['data1'] = $this->m_koperasi->read_jml_biaya_adm();
		$data['data2'] = $this->m_koperasi->read_jml_bunga();
		$data['data3'] = $this->m_koperasi->read_jml_tagihan();
		$data['data4'] = $this->m_koperasi->read_jml_angsuran();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
		$data['pages'] = $this->load->view('pages/Laporan/v_laba_rugi',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
}
