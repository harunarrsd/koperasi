<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pinjaman extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	// Controller Data Pengajuan 
	public function data_pengajuan()	{
		$data['data'] = $this->m_koperasi->read_data_pengajuan()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Pinjaman/v_data_pengajuan',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	// Controller Data Pinjaman 
	public function data_pinjaman()	{
		$data['data'] = $this->m_koperasi->read_data_pinjaman()->result();
		$data['data1'] = $this->m_koperasi->get_tbl_anggota()->result();
		// $data['data2'] = $this->general_m->get_jml_bayar()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Pinjaman/v_data_pinjaman',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function edit_data_pinjaman($id)	{
		$data['data'] = $this->m_koperasi->edit_data_pinjaman($id)->result();
		$data['data1'] = $this->m_koperasi->get_tbl_anggota()->result();
		$data['data2'] = $this->m_koperasi->read_data_barang()->result();
		$data['data3'] = $this->m_koperasi->read_lama_angsuran()->result();
		$data['data4'] = $this->m_koperasi->get_tbl_kas()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Pinjaman/v_edit_data_pinjaman',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	
	public function detail_data_pinjaman($id)	{
		$data['data'] =  $this->m_koperasi->detail_data_pinjaman($id)->result();
		$data['data1'] =  $this->m_koperasi->detail_data_angsuran($id)->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Pinjaman/v_detail_data_pinjaman',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_data_pinjaman(){
		$id = $this->input->post('id');
		$update_data = date('Y-m-d H:i');
		$tgl_pinjam = $this->input->post('tgl_pinjam');
		$lama_angsuran = $this->input->post('lama_angsuran');
		$jumlah = $this->input->post('jumlah');
		$bunga = $this->input->post('bunga');
		$biaya_adm = $this->input->post('biaya_adm');
		$kas_id = $this->input->post('kas_id');
		$keterangan = $this->input->post('keterangan');
		$username = $this->session->userdata('name');
	
		$data = array(
			'tgl_pinjam' => $tgl_pinjam,
			'lama_angsuran' => $lama_angsuran,
			'jumlah' => $jumlah,
			'bunga' => $bunga/100,
			'biaya_adm' => $biaya_adm,
			'kas_id' => $kas_id,
			'update_data' => $update_data,
			'keterangan' => $keterangan,
			'user_name' => $username
		);
	
		$where =  $id;
		$this->m_koperasi->update_data_pinjaman($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Pinjaman ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Pinjaman/data_pinjaman'));
	}
	
	public function create_data_pinjaman(){
		$data['data1'] = $this->m_koperasi->get_tbl_anggota()->result();
		$data['data2'] = $this->m_koperasi->read_data_barang1()->result();
		$data['data3'] = $this->m_koperasi->read_lama_angsuran()->result();
		$data['data4'] = $this->m_koperasi->get_tbl_kas()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Pinjaman/v_create_data_pinjaman',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function create_data_pinjaman_action(){
		$tgl_pinjam = $this->input->post('tgl_pinjam');
		$anggota_id = $this->input->post('anggota_id');
		$barang_id = $this->input->post('barang_id');
		$lama_angsuran = $this->input->post('lama_angsuran');
		$jumlah = $this->input->post('jumlah');
		$bunga = $this->input->post('bunga');
		$biaya_adm = $this->input->post('biaya_adm');
		$kas_id = $this->input->post('kas_id');
		$keterangan = $this->input->post('keterangan');
		$username = $this->session->userdata('name');
	
		$data = array(
			'tgl_pinjam' => $tgl_pinjam,
			'anggota_id' => $anggota_id,
			'barang_id' => $barang_id,
			'lama_angsuran' => $lama_angsuran,
			'jumlah' => $jumlah,
			'bunga' => $bunga/100,
			'biaya_adm' => $biaya_adm,
			'dk' => 'K',
			'jns_trans' => '7',
			'kas_id' => $kas_id,
			'keterangan' => $keterangan,
			'user_name' => $username
		);
	
		$this->m_koperasi->create_data_pinjaman($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Pinjaman ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Pinjaman/data_pinjaman'));
	}

	public function delete_data_pinjaman($id){
		$where = $id;
		$this->m_koperasi->delete_data_pinjaman($where);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil hapus Data Pinjaman ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Pinjaman/data_pinjaman'));
	}

	// Controller Pelunasan Pinjaman
	public function pelunasan_pinjaman()	{
		$data['data'] = $this->m_koperasi->read_pelunasan_pinjaman()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Pinjaman/v_pelunasan_pinjaman',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
}
