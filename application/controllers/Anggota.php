<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	public function index()	{
		$data['title'] = 'Anggota';
		$id_anggota = $this->session->userdata('id_anggota');
		$data['data'] = $this->m_koperasi->get_data_anggota($id_anggota)->result();
		$data['data1'] = $this->m_koperasi->get_simpanan_pokok($id_anggota)->result();
		$data['data2'] = $this->m_koperasi->get_simpanan_wajib($id_anggota)->result();
		$data['data3'] = $this->m_koperasi->get_simpanan_sukarela($id_anggota)->result();
		$data['data4'] = $this->m_koperasi->get_pinjaman($id_anggota)->result();
        $data['pages'] = $this->load->view('pages/Anggota/v_beranda',$data,true);
		$this->load->view('master_anggota',array('main'=>$data));
	}
	function data_pengajuan()	{
		$data['title'] = 'Anggota';
		$id_anggota = $this->session->userdata('id_anggota');
		$data['data'] = $this->m_koperasi->get_data_anggota($id_anggota)->result();
		$data['data1'] = $this->m_koperasi->get_pengajuan($id_anggota)->result();
        $data['pages'] = $this->load->view('pages/Anggota/v_data_pengajuan',$data,true);
		$this->load->view('master_anggota',array('main'=>$data));
	}
	function create_data_pengajuan()	{
		$data['title'] = 'Anggota';
		$id_anggota = $this->session->userdata('id_anggota');
		$data['data'] = $this->m_koperasi->get_data_anggota($id_anggota)->result();
		$data['data1'] = $this->m_koperasi->get_pengajuan($id_anggota)->result();
		$data['data3'] = $this->m_koperasi->read_lama_angsuran()->result();
        $data['pages'] = $this->load->view('pages/Anggota/v_create_data_pengajuan',$data,true);
		$this->load->view('master_anggota',array('main'=>$data));
	}
	public function create_data_pengajuan_action(){
		$anggota_id = $this->session->userdata('id_anggota');
		$jenis = $this->input->post('jenis');
		$nominal = $this->input->post('nominal');
		$lama_angsuran = $this->input->post('lama_angsuran');
		$keterangan = $this->input->post('keterangan');
		$this->db->select_max('no_ajuan');
		$this->db->from('tbl_pengajuan');
		$query = $this->db->get();
		$no_ajuan = 1;
		if($query->num_rows() > 0) {
			$row = $query->row();
			$no_ajuan = $row->no_ajuan + 1;
		}
		// ajuan_id
		$ajuan_id = '';
		if($jenis == 'Biasa') {
			$ajuan_id .= 'B';
		}
		if($jenis == 'Darurat') {
			$lama_angsuran = 1;
			$ajuan_id .= 'D';
		}
		if($jenis == 'Barang') {
			$ajuan_id .= 'BR';
		}
		if(date("d") >= 21) {
			$ajuan_id .= '.' . substr(date("Y", strtotime("+1 month")), 2, 2);
			$ajuan_id .= '.' . date("m", strtotime("+1 month"));
		} else {
			$ajuan_id .= '.' . substr(date("Y"), 2, 2);
			$ajuan_id .= '.' . date("m");
		}
		$ajuan_id .= '.' . sprintf("%03d", $no_ajuan);
		
		$data = array(
			'no_ajuan' => $no_ajuan,
			'ajuan_id' => $ajuan_id,
			'anggota_id' => $anggota_id,
			'jenis' => $jenis,
			'lama_ags' => $lama_angsuran,
			'nominal' => $nominal,
			'keterangan' => $keterangan,
			'tgl_input'	=> date('Y-m-d H:i:s'),
			'tgl_update' => date('Y-m-d H:i:s'),
			'status' => 0
		);
		
		$this->m_koperasi->create_data_pengajuan($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil mengajukan pinjaman ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Anggota/data_pengajuan'));
	}
	function laporan_simpanan()	{
		$data['title'] = 'Anggota';
		$id_anggota = $this->session->userdata('id_anggota');
		$data['data'] = $this->m_koperasi->get_data_anggota($id_anggota)->result();
		$data['data1'] = $this->m_koperasi->get_pengajuan($id_anggota)->result();
        $data['pages'] = $this->load->view('pages/Anggota/v_laporan_simpanan',$data,true);
		$this->load->view('master_anggota',array('main'=>$data));
	}
	function laporan_pinjaman()	{
		$data['title'] = 'Anggota';
		$id_anggota = $this->session->userdata('id_anggota');
		$data['data'] = $this->m_koperasi->get_data_anggota($id_anggota)->result();
		$data['data1'] = $this->m_koperasi->get_pengajuan($id_anggota)->result();
        $data['pages'] = $this->load->view('pages/Anggota/v_laporan_simpanan',$data,true);
		$this->load->view('master_anggota',array('main'=>$data));
	}
	function laporan_pembayaran()	{
		$data['title'] = 'Anggota';
		$id_anggota = $this->session->userdata('id_anggota');
		$data['data'] = $this->m_koperasi->get_data_anggota($id_anggota)->result();
		$data['data1'] = $this->m_koperasi->get_pengajuan($id_anggota)->result();
        $data['pages'] = $this->load->view('pages/Anggota/v_laporan_pembayaran',$data,true);
		$this->load->view('master_anggota',array('main'=>$data));
	}
}
