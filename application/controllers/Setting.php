<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}
	// Controller Identitas Koperasi
	public function identitas_koperasi()	{
		$data['data'] = $this->m_koperasi->read_identitas_koperasi()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Setting/v_identitas_koperasi',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_identitas_koperasi(){
		$id_nama_lembaga = $this->input->post('id_nama_lembaga');
		$nama_lembaga = $this->input->post('nama_lembaga');
		$data = array(
			'opsi_val' => $nama_lembaga,
		);
		$where =  $id_nama_lembaga;
		$this->m_koperasi->update_identitas_koperasi($where,$data);
		
		$id_nama_ketua = $this->input->post('id_nama_ketua');
		$nama_ketua = $this->input->post('nama_ketua');
		$data = array(
			'opsi_val' => $nama_ketua,
		);
		$where =  $id_nama_ketua;
		$this->m_koperasi->update_identitas_koperasi($where,$data);

		$id_hp_ketua = $this->input->post('id_hp_ketua');
		$hp_ketua = $this->input->post('hp_ketua');
		$data = array(
			'opsi_val' => $hp_ketua,
		);
		$where =  $id_hp_ketua;
		$this->m_koperasi->update_identitas_koperasi($where,$data);

		$id_alamat = $this->input->post('id_alamat');
		$alamat = $this->input->post('alamat');
		$data = array(
			'opsi_val' => $alamat,
		);
		$where =  $id_alamat;
		$this->m_koperasi->update_identitas_koperasi($where,$data);

		$id_telepon = $this->input->post('id_telepon');
		$telepon = $this->input->post('telepon');
		$data = array(
			'opsi_val' => $telepon,
		);
		$where =  $id_telepon;
		$this->m_koperasi->update_identitas_koperasi($where,$data);

		$id_kota = $this->input->post('id_kota');
		$kota = $this->input->post('kota');
		$data = array(
			'opsi_val' => $kota,
		);
		$where =  $id_kota;
		$this->m_koperasi->update_identitas_koperasi($where,$data);

		$id_email = $this->input->post('id_email');
		$email = $this->input->post('email');
		$data = array(
			'opsi_val' => $email,
		);
		$where =  $id_email;
		$this->m_koperasi->update_identitas_koperasi($where,$data);

		$id_web = $this->input->post('id_web');
		$web = $this->input->post('web');
		$data = array(
			'opsi_val' => $web,
		);
		$where =  $id_web;
		$this->m_koperasi->update_identitas_koperasi($where,$data);
		
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Identitas Koperasi ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Setting/identitas_koperasi'));
	}

	// Controller Suku Bunga
	public function suku_bunga()	{
		$data['data'] = $this->m_koperasi->read_suku_bunga()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Setting/v_suku_bunga',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_suku_bunga(){
		$id_bg_tab = $this->input->post('id_bg_tab');
		$bg_tab = $this->input->post('bg_tab');
		$data = array(
			'opsi_val' => $bg_tab,
		);
		$where =  $id_bg_tab;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_bg_pinjam = $this->input->post('id_bg_pinjam');
		$bg_pinjam = $this->input->post('bg_pinjam');
		$data = array(
			'opsi_val' => $bg_pinjam,
		);
		$where =  $id_bg_pinjam;
		$this->m_koperasi->update_suku_bunga($where,$data);
		
		$id_biaya_adm = $this->input->post('id_biaya_adm');
		$biaya_adm = $this->input->post('biaya_adm');
		$data = array(
			'opsi_val' => $biaya_adm,
		);
		$where =  $id_biaya_adm;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_denda = $this->input->post('id_denda');
		$denda = $this->input->post('denda');
		$data = array(
			'opsi_val' => $denda,
		);
		$where =  $id_denda;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_denda_hari = $this->input->post('id_denda_hari');
		$denda_hari = $this->input->post('denda_hari');
		$data = array(
			'opsi_val' => $denda_hari,
		);
		$where =  $id_denda_hari;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_dana_cadangan = $this->input->post('id_dana_cadangan');
		$dana_cadangan = $this->input->post('dana_cadangan');
		$data = array(
			'opsi_val' => $dana_cadangan,
		);
		$where =  $id_dana_cadangan;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_jasa_anggota = $this->input->post('id_jasa_anggota');
		$jasa_anggota = $this->input->post('jasa_anggota');
		$data = array(
			'opsi_val' => $jasa_anggota,
		);
		$where =  $id_jasa_anggota;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_dana_pengurus = $this->input->post('id_dana_pengurus');
		$dana_pengurus = $this->input->post('dana_pengurus');
		$data = array(
			'opsi_val' => $dana_pengurus,
		);
		$where =  $id_dana_pengurus;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_dana_karyawan = $this->input->post('id_dana_karyawan');
		$dana_pend = $this->input->post('dana_pend');
		$data = array(
			'opsi_val' => $dana_pend,
		);
		$where =  $id_dana_karyawan;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_dana_pend = $this->input->post('id_dana_pend');
		$dana_pend = $this->input->post('dana_pend');
		$data = array(
			'opsi_val' => $dana_pend,
		);
		$where =  $id_dana_pend;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_dana_sosial = $this->input->post('id_dana_sosial');
		$dana_sosial = $this->input->post('dana_sosial');
		$data = array(
			'opsi_val' => $dana_sosial,
		);
		$where =  $id_dana_sosial;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_jasa_usaha = $this->input->post('id_jasa_usaha');
		$jasa_usaha = $this->input->post('jasa_usaha');
		$data = array(
			'opsi_val' => $jasa_usaha,
		);
		$where =  $id_jasa_usaha;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_jasa_modal = $this->input->post('id_jasa_modal');
		$jasa_modal = $this->input->post('jasa_modal');
		$data = array(
			'opsi_val' => $jasa_modal,
		);
		$where =  $id_jasa_modal;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_pjk_pph = $this->input->post('id_pjk_pph');
		$pjk_pph = $this->input->post('pjk_pph');
		$data = array(
			'opsi_val' => $pjk_pph,
		);
		$where =  $id_pjk_pph;
		$this->m_koperasi->update_suku_bunga($where,$data);

		$id_pinjaman_bunga_tipe = $this->input->post('id_pinjaman_bunga_tipe');
		$pinjaman_bunga_tipe = $this->input->post('pinjaman_bunga_tipe');
		$data = array(
			'opsi_val' => $pinjaman_bunga_tipe,
		);
		$where =  $id_pinjaman_bunga_tipe;
		$this->m_koperasi->update_suku_bunga($where,$data);
		
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Biaya dan Administrasi ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Setting/suku_bunga'));
	}
}
