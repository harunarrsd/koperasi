<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->model('m_koperasi');
	}

	public function index()	{
		$data['data1'] = $this->m_koperasi->read_tbl_pekerjaan()->result();
		$data['title'] = 'Koperasi';
        $data['pages'] = $this->load->view('pages/v_register',array('main'=>$data),true);
		$this->load->view('main_login',array('main'=>$data));
	}

	public function create(){
		$filename = date("YmdHis");
		$config = array(
			'upload_path'=>'upload/foto_profil/',
			'allowed_types'=>'jpg|png|jpeg',
			'max_size'=>2086,
			'file_name'=>$filename
		);
		$this->upload->initialize($config);
		$this->upload->do_upload('file_pic');
		$finfo = $this->upload->data();
		$file_pic = $finfo['file_name'];

		$nama = $this->input->post('nama');
		$identitas = $this->input->post('identitas');
		$jk = $this->input->post('jk');
		$alamat = $this->input->post('alamat');
		$kota = $this->input->post('kota');
		$jabatan = $this->input->post('jabatan');
		$departement = $this->input->post('departement');
		$aktif = $this->input->post('aktif');
		$tmp_lahir = $this->input->post('tmp_lahir');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$status = $this->input->post('status');
		$agama = $this->input->post('agama');
		$pekerjaan = $this->input->post('pekerjaan');
		$notelp = $this->input->post('notelp');
		$pass_word = md5($this->input->post('pass_word'));

		$data_pengguna = array(
			'u_name' => $identitas,
			'pass_word' => $pass_word,
			'aktif' => $aktif,
			'level' => 'anggota'
		);
		$this->m_koperasi->create_data_pengguna($data_pengguna);

		$user_id=$this->db->insert_id();
		
		$data = array(
			'nama' => $nama,
			'identitas' => $identitas,
			'jk' => $jk,
			'alamat' => $alamat,
			'kota' => $kota,
			'jabatan' => $jabatan,
			'departement' => $departement,
			'tgl_daftar' => date("Y-m-d H:i:s"),
			'aktif' => $aktif,
			'tmp_lahir' => $tmp_lahir,
			'tgl_lahir' => $tgl_lahir,
			'status' => $status,
			'agama' => $agama,
			'notelp' => $notelp,
			'pekerjaan' => $pekerjaan,
			'pass_word' => $pass_word,
			'file_pic' => $file_pic,
			'user_id' => $user_id
		);
		
		$this->m_koperasi->create_data_anggota($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Register Anggota Berhasil ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect('.');
	}
}
