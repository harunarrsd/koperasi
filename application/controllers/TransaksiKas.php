<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TransaksiKas extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	// Controller Data pengeluaran Kas 
	public function transaksi_kas_pemasukan()	{
		$data['data'] = $this->m_koperasi->read_transaksi_kas_pemasukan1()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Transaksi_kas/v_transaksi_kas_pemasukan',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function edit_transaksi_kas_pemasukan($id)	{
		$data['data'] = $this->m_koperasi->edit_transaksi_kas_pemasukan($id)->result();
		$data['data2'] = $this->m_koperasi->read_data_kas()->result();
		$data['data1'] = $this->m_koperasi->read_jns_akun()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Transaksi_kas/v_edit_transaksi_kas_pemasukan',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_transaksi_kas_pemasukan(){
		$id = $this->input->post('id');
		$tgl_catat = $this->input->post('tgl_catat');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$jns_trans = $this->input->post('jns_trans');
		$untuk_kas_id = $this->input->post('untuk_kas_id');
		$update_data = date('Y-m-d H:i');
		$username = $this->session->userdata('name');
	
		$data = array(
			'tgl_catat' => $tgl_catat,
			'jumlah' => $jumlah,
			'keterangan' => $keterangan,
			'untuk_kas_id' => $untuk_kas_id,
			'jns_trans' => $jns_trans,
			'update_data' => $update_data,
			'user_name' => $username
		);
	
		$where =  $id;
		$this->m_koperasi->update_transaksi_kas_pemasukan($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Pemasukan Transaksi Kas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('TransaksiKas/transaksi_kas_pemasukan'));
	}
	
	public function create_transaksi_kas_pemasukan(){
		$data['data1'] = $this->m_koperasi->read_data_kas()->result();
		$data['data2'] = $this->m_koperasi->read_jns_akun()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Transaksi_kas/v_create_transaksi_kas_pemasukan',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function create_transaksi_kas_pemasukan_action(){
		$tgl_catat = $this->input->post('tgl_catat');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$jns_trans = $this->input->post('jns_trans');
		$untuk_kas_id = $this->input->post('untuk_kas_id');
		$username = $this->session->userdata('name');
	
		$data = array(
			'tgl_catat' => $tgl_catat,
			'jumlah' => $jumlah,
			'keterangan' => $keterangan,
			'dk' => 'D',
			'akun' => 'Pemasukan',
			'untuk_kas_id' => $untuk_kas_id,
			'jns_trans' => $jns_trans,
			'user_name' => $username
		);
	
		$this->m_koperasi->create_transaksi_kas_pemasukan($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Pemasukan Transaksi Kas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('TransaksiKas/transaksi_kas_pemasukan'));
	}

	function delete_transaksi_kas_pemasukan($id){
		$where = $id;
		$this->m_koperasi->delete_transaksi_kas_pemasukan($where);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil hapus Data Pemasukan Transaksi Kas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('TransaksiKas/transaksi_kas_pemasukan'));
	}
	
	// Controller Data Pengeluaran Kas 
	public function transaksi_kas_pengeluaran()	{
		$data['data'] = $this->m_koperasi->read_transaksi_kas_pengeluaran1()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Transaksi_kas/v_transaksi_kas_pengeluaran',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function edit_transaksi_kas_pengeluaran($id)	{
		$data['data'] = $this->m_koperasi->edit_transaksi_kas_pengeluaran($id)->result();
		$data['data2'] = $this->m_koperasi->read_data_kas()->result();
		$data['data1'] = $this->m_koperasi->read_jns_akun()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Transaksi_kas/v_edit_transaksi_kas_pengeluaran',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_transaksi_kas_pengeluaran(){
		$id = $this->input->post('id');
		$tgl_catat = $this->input->post('tgl_catat');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$jns_trans = $this->input->post('jns_trans');
		$dari_kas_id = $this->input->post('dari_kas_id');
		$update_data = date('Y-m-d H:i');
		$username = $this->session->userdata('name');
	
		$data = array(
			'tgl_catat' => $tgl_catat,
			'jumlah' => $jumlah,
			'keterangan' => $keterangan,
			'dari_kas_id' => $dari_kas_id,
			'jns_trans' => $jns_trans,
			'update_data' => $update_data,
			'user_name' => $username
		);
	
		$where =  $id;
		$this->m_koperasi->update_transaksi_kas_pengeluaran($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Pengeluaran Transaksi Kas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('TransaksiKas/transaksi_kas_pengeluaran'));
	}
	
	public function create_transaksi_kas_pengeluaran(){
		$data['data2'] = $this->m_koperasi->read_data_kas()->result();
		$data['data1'] = $this->m_koperasi->read_jns_akun()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Transaksi_kas/v_create_transaksi_kas_pengeluaran',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function create_transaksi_kas_pengeluaran_action(){
		$tgl_catat = $this->input->post('tgl_catat');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$jns_trans = $this->input->post('jns_trans');
		$dari_kas_id = $this->input->post('dari_kas_id');
		$username = $this->session->userdata('name');
	
		$data = array(
			'tgl_catat' => $tgl_catat,
			'jumlah' => $jumlah,
			'keterangan' => $keterangan,
			'dk' => 'K',
			'akun' => 'Pengeluaran',
			'dari_kas_id' => $dari_kas_id,
			'jns_trans' => $jns_trans,
			'user_name' => $username
		);
	
		$this->m_koperasi->create_transaksi_kas_pengeluaran($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Pengeluaran Transaksi Kas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('TransaksiKas/transaksi_kas_pengeluaran'));
	}

	function delete_transaksi_kas_pengeluaran($id){
		$where = $id;
		$this->m_koperasi->delete_transaksi_kas_pengeluaran($where);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil hapus Data Pengeluaran Transaksi Kas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('TransaksiKas/transaksi_kas_pengeluaran'));
	}
	
	// Controller Data Transfer Kas 
	public function transaksi_kas_transfer()	{
		$data['data'] = $this->m_koperasi->read_transaksi_kas_transfer1()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Transaksi_kas/v_transaksi_kas_transfer',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function edit_transaksi_kas_transfer($id)	{
		$data['data'] = $this->m_koperasi->edit_transaksi_kas_transfer($id)->result();
		$data['data2'] = $this->m_koperasi->read_data_kas()->result();
		$data['data1'] = $this->m_koperasi->read_jns_akun()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Transaksi_kas/v_edit_transaksi_kas_transfer',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_transaksi_kas_transfer(){
		$id = $this->input->post('id');
		$tgl_catat = $this->input->post('tgl_catat');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$untuk_kas_id = $this->input->post('untuk_kas_id');
		$dari_kas_id = $this->input->post('dari_kas_id');
		$update_data = date('Y-m-d H:i');
		$username = $this->session->userdata('name');
	
		$data = array(
			'tgl_catat' => $tgl_catat,
			'jumlah' => $jumlah,
			'keterangan' => $keterangan,
			'untuk_kas_id' => $untuk_kas_id,
			'dari_kas_id' => $dari_kas_id,
			'update_data' => $update_data,
			'user_name' => $username
		);
	
		$where =  $id;
		$this->m_koperasi->update_transaksi_kas_transfer($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Transaksi Transfer AntarKas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('TransaksiKas/transaksi_kas_transfer'));
	}
	
	public function create_transaksi_kas_transfer(){
		$data['data2'] = $this->m_koperasi->read_data_kas()->result();
		$data['data1'] = $this->m_koperasi->read_jns_akun()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Transaksi_kas/v_create_transaksi_kas_transfer',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function create_transaksi_kas_transfer_action(){
		$tgl_catat = $this->input->post('tgl_catat');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$dari_kas_id = $this->input->post('dari_kas_id');
		$untuk_kas_id = $this->input->post('untuk_kas_id');
		$username = $this->session->userdata('name');
	
		$data = array(
			'tgl_catat' => $tgl_catat,
			'jumlah' => $jumlah,
			'keterangan' => $keterangan,
			'akun' => 'Transfer',
			'dari_kas_id' => $dari_kas_id,
			'untuk_kas_id' => $untuk_kas_id,
			'jns_trans' => '110',
			'user_name' => $username
		);
	
		$this->m_koperasi->create_transaksi_kas_transfer($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Transaksi Transfer AntarKas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('TransaksiKas/transaksi_kas_transfer'));
	}

	function delete_transaksi_kas_transfer($id){
		$where = $id;
		$this->m_koperasi->delete_transaksi_kas_transfer($where);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil hapus Data Transaksi Transfer AntarKas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('TransaksiKas/transaksi_kas_transfer'));
	}
}
