<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterData extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}
	// Controller Jenis Simpanan
	public function jns_simpanan()	{
		$data['data'] = $this->m_koperasi->read_jns_pinjaman()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_jenis_simpanan',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function edit_jns_simpanan($id)	{
		$data['data'] = $this->m_koperasi->edit_jns_pinjaman($id)->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_edit_jenis_simpanan',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_jns_simpanan(){
		$id = $this->input->post('id');
		$jns_simpanan = $this->input->post('jns_simpanan');
		$jumlah = $this->input->post('jumlah');
		$tampil = $this->input->post('tampil');
	
		$data = array(
			'jns_simpan' => $jns_simpanan,
			'jumlah' => $jumlah,
			'tampil' => $tampil
		);
	
		$where =  $id;
		$this->m_koperasi->update_jns_pinjaman($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Jenis Simpanan ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/jns_simpanan'));
	}
	
	// Controller Jenis Akun
	public function jns_akun()	{
		$data['data'] = $this->m_koperasi->read_jns_akun()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_jenis_akun',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	
	public function edit_jns_akun($id)	{
		$data['data'] = $this->m_koperasi->edit_jns_akun($id)->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_edit_jenis_akun',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_jns_akun(){
		$id = $this->input->post('id');
		$kd_aktiva = $this->input->post('kd_aktiva');
		$jns_trans = $this->input->post('jns_trans');
		$akun = $this->input->post('akun');
		$pemasukan = $this->input->post('pemasukan');
		$pengeluaran = $this->input->post('pengeluaran');
		$aktif = $this->input->post('aktif');
		$laba_rugi = $this->input->post('laba_rugi');
		
		$data = array(
			'kd_aktiva' => $kd_aktiva,
			'jns_trans' => $jns_trans,
			'akun' => $akun,
			'pemasukan' => $pemasukan,
			'pengeluaran' => $pengeluaran,
			'aktif' => $aktif,
			'laba_rugi' => $laba_rugi,
		);
		
		$where =  $id;
		$this->m_koperasi->update_jns_akun($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Jenis Akun ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/jns_akun'));
	}
	
	public function create_jns_akun(){
		$data['data'] = $this->m_koperasi->read_jns_akun()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_create_jenis_akun',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function create_jns_akun_action(){
		$kd_aktiva = $this->input->post('kd_aktiva');
		$jns_trans = $this->input->post('jns_trans');
		$akun = $this->input->post('akun');
		$pemasukan = $this->input->post('pemasukan');
		$pengeluaran = $this->input->post('pengeluaran');
		$aktif = $this->input->post('aktif');
		$laba_rugi = $this->input->post('laba_rugi');
	
		$data = array(
			'kd_aktiva' => $kd_aktiva,
			'jns_trans' => $jns_trans,
			'akun' => $akun,
			'laba_rugi' => $laba_rugi,
			'pemasukan' => $pemasukan,
			'pengeluaran' => $pengeluaran,
			'aktif' => $aktif
		);
	
		$this->m_koperasi->create_jns_akun($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Jenis Akun ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/jns_akun'));
	}

	// Controller Data Kas
	public function data_kas()	{
		$data['data'] = $this->m_koperasi->read_data_kas()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_data_kas',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function edit_data_kas($id)	{
		$data['data'] = $this->m_koperasi->edit_data_kas($id)->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_edit_data_kas',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_data_kas(){
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$aktif = $this->input->post('aktif');
		$tmpl_simpan = $this->input->post('tmpl_simpan');
		$tmpl_penarikan = $this->input->post('tmpl_penarikan');
		$tmpl_pinjaman = $this->input->post('tmpl_pinjaman');
		$tmpl_bayar = $this->input->post('tmpl_bayar');
		$tmpl_pemasukan = $this->input->post('tmpl_pemasukan');
		$tmpl_pengeluaran = $this->input->post('tmpl_pengeluaran');
		$tmpl_transfer = $this->input->post('tmpl_transfer');
	
		$data = array(
			'nama' => $nama,
			'aktif' => $aktif,
			'tmpl_simpan' => $tmpl_simpan,
			'tmpl_penarikan' => $tmpl_penarikan,
			'tmpl_pinjaman' => $tmpl_pinjaman,
			'tmpl_bayar' => $tmpl_bayar,
			'tmpl_pemasukan' => $tmpl_pemasukan,
			'tmpl_pengeluaran' => $tmpl_pengeluaran,
			'tmpl_transfer' => $tmpl_transfer,
		);
	
		$where =  $id;
		$this->m_koperasi->update_data_kas($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Jenis Kas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_kas'));
	}
	
	public function create_data_kas(){
		$data['data'] = $this->m_koperasi->read_data_kas()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_create_data_kas',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function create_data_kas_action(){
		$nama = $this->input->post('nama');
		$aktif = $this->input->post('aktif');
		$tmpl_simpan = $this->input->post('tmpl_simpan');
		$tmpl_penarikan = $this->input->post('tmpl_penarikan');
		$tmpl_pinjaman = $this->input->post('tmpl_pinjaman');
		$tmpl_bayar = $this->input->post('tmpl_bayar');
		$tmpl_pemasukan = $this->input->post('tmpl_pemasukan');
		$tmpl_pengeluaran = $this->input->post('tmpl_pengeluaran');
		$tmpl_transfer = $this->input->post('tmpl_transfer');
	
		$data = array(
			'nama' => $nama,
			'aktif' => $aktif,
			'tmpl_simpan' => $tmpl_simpan,
			'tmpl_penarikan' => $tmpl_penarikan,
			'tmpl_pinjaman' => $tmpl_pinjaman,
			'tmpl_bayar' => $tmpl_bayar,
			'tmpl_pemasukan' => $tmpl_pemasukan,
			'tmpl_pengeluaran' => $tmpl_pengeluaran,
			'tmpl_transfer' => $tmpl_transfer,
		);
	
		$this->m_koperasi->create_data_kas($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Jenis Kas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_kas'));
	}

	function delete_data_kas($id){
		$where = $id;
		$this->m_koperasi->delete_data_kas($where);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil hapus Data Jenis Kas ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_kas'));
	}

	// Controller Lama Angsuran
	public function lama_angsuran()	{
		$data['data'] = $this->m_koperasi->read_lama_angsuran()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_lama_angsuran',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function edit_lama_angsuran($id)	{
		$data['data'] = $this->m_koperasi->edit_lama_angsuran($id)->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_edit_lama_angsuran',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_lama_angsuran(){
		$id = $this->input->post('id');
		$ket = $this->input->post('ket');
		$aktif = $this->input->post('aktif');
	
		$data = array(
			'ket' => $ket,
			'aktif' => $aktif,
		);
	
		$where =  $id;
		$this->m_koperasi->update_lama_angsuran($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Lama Angsuran ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/lama_angsuran'));
	}
	
	public function create_lama_angsuran(){
		$data['data'] = $this->m_koperasi->read_lama_angsuran()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_create_lama_angsuran',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	
	public function create_lama_angsuran_action(){
		$ket = $this->input->post('ket');
		$aktif = $this->input->post('aktif');
		
		$data = array(
			'ket' => $ket,
			'aktif' => $aktif,
		);
		
		$this->m_koperasi->create_lama_angsuran($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Lama Angsuran ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/lama_angsuran'));
	}
	
	function delete_lama_angsuran($id){
		$where = $id;
		$this->m_koperasi->delete_lama_angsuran($where);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil hapus Data Lama Angsuran ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/lama_angsuran'));
	}
	
	// Controller Data Barang
	public function data_barang()	{
		$data['data'] = $this->m_koperasi->read_data_barang()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_data_barang',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	
	public function edit_data_barang($id)	{
		$data['data'] = $this->m_koperasi->edit_data_barang($id)->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_edit_data_barang',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	
	public function update_data_barang(){
		$id = $this->input->post('id');
		$nm_barang = $this->input->post('nm_barang');
		$type = $this->input->post('type');
		$merk = $this->input->post('merk');
		$harga = $this->input->post('harga');
		$jml_brg = $this->input->post('jml_brg');
		$ket = $this->input->post('ket');
		
		$data = array(
			'nm_barang' => $nm_barang,
			'type' => $type,
			'merk' => $merk,
			'harga' => $harga,
			'jml_brg' => $jml_brg,
			'ket' => $ket
		);
		
		$where =  $id;
		$this->m_koperasi->update_data_barang($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Barang ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_barang'));
	}
	
	public function create_data_barang(){
		$data['data'] = $this->m_koperasi->read_data_barang()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_create_data_barang',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	
	public function create_data_barang_action(){
		$nm_barang = $this->input->post('nm_barang');
		$type = $this->input->post('type');
		$merk = $this->input->post('merk');
		$harga = $this->input->post('harga');
		$jml_brg = $this->input->post('jml_brg');
		$ket = $this->input->post('ket');
		
		$data = array(
			'nm_barang' => $nm_barang,
			'type' => $type,
			'merk' => $merk,
			'harga' => $harga,
			'jml_brg' => $jml_brg,
			'ket' => $ket
		);
		
		$this->m_koperasi->create_data_barang($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Barang ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_barang'));
	}
	
	function delete_data_barang($id){
		$where = $id;
		$this->m_koperasi->delete_data_barang($where);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil hapus Data Barang ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_barang'));
	}
	
	// Controller Data Pengguna
	public function data_pengguna()	{
		$data['data'] = $this->m_koperasi->read_data_pengguna()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_data_pengguna',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	
	public function edit_data_pengguna($id)	{
		$data['data'] = $this->m_koperasi->edit_data_pengguna($id)->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_edit_data_pengguna',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	
	public function update_data_pengguna(){
		$id = $this->input->post('id');
		$u_name = $this->input->post('u_name');
		$level = $this->input->post('level');
		$aktif = $this->input->post('aktif');
		$old_pass_word = md5($this->input->post('old_pass_word')) ;
		// $new_pass_word = $this->input->post('new_pass_word');
		// $confirm_pass_word = $this->input->post('confirm_pass_word');
		
		$data = array(
			'u_name' => $u_name,
			'level' => $level,
			'aktif' => $aktif,
			'pass_word' => $old_pass_word,
			// 'new_pass_word' => $new_pass_word,
			// 'confirm_pass_word' => $confirm_pass_word
		);
		
		$where =  $id;
		$this->m_koperasi->update_data_pengguna($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Pengguna ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_pengguna'));
	}
	
	public function create_data_pengguna(){
		$data['data'] = $this->m_koperasi->read_data_pengguna()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_create_data_pengguna',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	
	public function create_data_pengguna_action(){
		$u_name = $this->input->post('u_name');
		$level = $this->input->post('level');
		$aktif = $this->input->post('aktif');
		$pass_word = md5($this->input->post('pass_word'));
		
		$data = array(
			'u_name' => $u_name,
			'level' => $level,
			'aktif' => $aktif,
			'pass_word' => $pass_word
		);
		
		$this->m_koperasi->create_data_pengguna($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Pengguna ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_pengguna'));
	}
	
	function delete_data_pengguna($id){
		$where = $id;
		$this->m_koperasi->delete_data_pengguna($where);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil hapus Data Pengguna ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_pengguna'));
	}

	// Controller Data Anggota
	public function data_anggota()	{
		$data['data'] = $this->m_koperasi->read_data_anggota()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_data_anggota',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function edit_data_anggota($id)	{
		$data['data'] = $this->m_koperasi->edit_data_anggota($id)->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_edit_data_anggota',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_data_anggota(){
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$identitas = $this->input->post('identitas');
		$jk = $this->input->post('jk');
		$alamat = $this->input->post('alamat');
		$kota = $this->input->post('kota');
		$jabatan = $this->input->post('jabatan');
		$departement = $this->input->post('departement');
		$tgl_daftar = $this->input->post('tgl_daftar');
		$aktif = $this->input->post('aktif');
		$tmp_lahir = $this->input->post('tmp_lahir');
		$status = $this->input->post('status');
		$agama = $this->input->post('agama');
		$pekerjaan = $this->input->post('pekerjaan');
		$notelp = $this->input->post('notelp');
		$pass_word = md5($this->input->post('pass_word'));
		$file_pic = $this->input->post('file_pic');
	
		$data = array(
			'nama' => $nama,
			'identitas' => $identitas,
			'jk' => $jk,
			'alamat' => $alamat,
			'kota' => $kota,
			'jabatan' => $jabatan,
			'departement' => $departement,
			'tgl_daftar' => $tgl_daftar,
			'aktif' => $aktif,
			'tmp_lahir' => $tmp_lahir,
			'status' => $status,
			'agama' => $agama,
			'notelp' => $notelp,
			'pekerjaan' => $pekerjaan,
			'pass_word' => $pass_word,
			'file_pic' => $file_pic
		);
	
		$where =  $id;
		$this->m_koperasi->update_data_anggota($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Anggota ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_anggota'));
	}
	
	public function create_data_anggota(){
		$data['data'] = $this->m_koperasi->read_data_anggota()->result();
		$data['data1'] = $this->m_koperasi->read_tbl_pekerjaan()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Master_data/v_create_data_anggota',$data,true);
		$this->load->view('master',array('main'=>$data));
	}
	
	public function create_data_anggota_action(){
		$filename = date("YmdHis");
		$config = array(
			'upload_path'=>'upload/foto_profil/',
			'allowed_types'=>'jpg|png|jpeg',
			'max_size'=>2086,
			'file_name'=>$filename
		);
		$this->upload->initialize($config);
		$this->upload->do_upload('file_pic');
		$finfo = $this->upload->data();
		$file_pic = $finfo['file_name'];

		$nama = $this->input->post('nama');
		$identitas = $this->input->post('identitas');
		$jk = $this->input->post('jk');
		$alamat = $this->input->post('alamat');
		$kota = $this->input->post('kota');
		$jabatan = $this->input->post('jabatan');
		$departement = $this->input->post('departement');
		$aktif = $this->input->post('aktif');
		$tmp_lahir = $this->input->post('tmp_lahir');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$status = $this->input->post('status');
		$agama = $this->input->post('agama');
		$pekerjaan = $this->input->post('pekerjaan');
		$notelp = $this->input->post('notelp');
		$pass_word = md5($this->input->post('pass_word'));

		$data_pengguna = array(
			'u_name' => $identitas,
			'pass_word' => $pass_word,
			'aktif' => $aktif,
			'level' => 'anggota'
		);
		$this->m_koperasi->create_data_pengguna($data_pengguna);

		$user_id=$this->db->insert_id();
		
		$data = array(
			'nama' => $nama,
			'identitas' => $identitas,
			'jk' => $jk,
			'alamat' => $alamat,
			'kota' => $kota,
			'jabatan' => $jabatan,
			'departement' => $departement,
			'tgl_daftar' => date("Y-m-d H:i:s"),
			'aktif' => $aktif,
			'tmp_lahir' => $tmp_lahir,
			'tgl_lahir' => $tgl_lahir,
			'status' => $status,
			'agama' => $agama,
			'notelp' => $notelp,
			'pekerjaan' => $pekerjaan,
			'pass_word' => $pass_word,
			'file_pic' => $file_pic,
			'user_id' => $user_id
		);
		
		$this->m_koperasi->create_data_anggota($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Anggota ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_anggota'));
	}
	
	function delete_data_anggota($id){
		$where = $id;
		$this->m_koperasi->delete_data_anggota($where);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil hapus Data Anggota ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('MasterData/data_anggota'));
	}
}
