<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simpanan extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('m_koperasi');
		if ($this->session->userdata('udhmasuk')==false) {
			redirect('.');
		}
	}

	// Controller Simpanan Setoran 
	public function setoran_tunai()	{
		$data['data'] = $this->m_koperasi->read_setoran_tunai()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Simpanan/v_setoran_tunai',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function edit_setoran_tunai($id)	{
		$data['data'] = $this->m_koperasi->edit_setoran_tunai($id)->result();
		$data['data1'] = $this->m_koperasi->get_tbl_anggota()->result();
		$data['data2'] = $this->m_koperasi->get_tbl_jns_simpanan()->result();
		$data['data3'] = $this->m_koperasi->get_tbl_kas()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Simpanan/v_edit_setoran_tunai',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_setoran_tunai(){
		$id = $this->input->post('id');
		$update_data = date('Y-m-d H:i');
		$tgl_transaksi = $this->input->post('tgl_transaksi');
		$nama_penyetor = $this->input->post('nama_penyetor');
		$no_identitas = $this->input->post('no_identitas');
		$alamat = $this->input->post('alamat');
		$jenis_id = $this->input->post('jenis_id');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$kas_id = $this->input->post('kas_id');
		$username = $this->session->userdata('name');
	
		$data = array(
			'update_data' => $update_data,
			'tgl_transaksi' => $tgl_transaksi,
			'nama_penyetor' => $nama_penyetor,
			'no_identitas' => $no_identitas,
			'alamat' => $alamat,
			'jenis_id' => $jenis_id,
			'jumlah' => $jumlah,
			'keterangan' => $keterangan,
			'kas_id' => $kas_id,
			'user_name' => $username
		);
	
		$where =  $id;
		$this->m_koperasi->update_setoran_tunai($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Setoran Tunai ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Simpanan/setoran_tunai'));
	}
	
	public function create_setoran_tunai(){
		$data['data1'] = $this->m_koperasi->get_tbl_anggota()->result();
		$data['data2'] = $this->m_koperasi->get_tbl_jns_simpanan()->result();
		$data['data3'] = $this->m_koperasi->get_tbl_kas()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Simpanan/v_create_setoran_tunai',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function create_setoran_tunai_action(){
		$tgl_transaksi = $this->input->post('tgl_transaksi');
		$nama_penyetor = $this->input->post('nama_penyetor');
		$no_identitas = $this->input->post('no_identitas');
		$alamat = $this->input->post('alamat');
		$anggota_id = $this->input->post('anggota_id');
		$jenis_id = $this->input->post('jenis_id');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$kas_id = $this->input->post('kas_id');
		$username = $this->session->userdata('name');
	
		$data = array(
			'tgl_transaksi' => $tgl_transaksi,
			'nama_penyetor' => $nama_penyetor,
			'no_identitas' => $no_identitas,
			'alamat' => $alamat,
			'anggota_id' => $anggota_id,
			'jenis_id' => $jenis_id,
			'jumlah' => $jumlah,
			'keterangan' => $keterangan,
			'akun' => 'Setoran',
			'dk' => 'D',
			'kas_id' => $kas_id,
			'user_name' => $username
		);
	
		$this->m_koperasi->create_setoran_tunai($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Setoran Tunai ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Simpanan/setoran_tunai'));
	}

	function delete_setoran_tunai($id){
		$where = $id;
		$this->m_koperasi->delete_setoran_tunai($where);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil hapus Data Setoran Tunai ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Simpanan/setoran_tunai'));
	}

	// Controller Simpanan Penarikan 
	public function penarikan_tunai()	{
		$data['data'] = $this->m_koperasi->read_penarikan_tunai()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Simpanan/v_penarikan_tunai',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function edit_penarikan_tunai($id)	{
		$data['data'] = $this->m_koperasi->edit_penarikan_tunai($id)->result();
		$data['data1'] = $this->m_koperasi->get_tbl_anggota()->result();
		$data['data2'] = $this->m_koperasi->get_tbl_jns_simpanan()->result();
		$data['data3'] = $this->m_koperasi->get_tbl_kas()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Simpanan/v_edit_penarikan_tunai',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function update_penarikan_tunai(){
		$id = $this->input->post('id');
		$update_data = date('Y-m-d H:i');
		$tgl_transaksi = $this->input->post('tgl_transaksi');
		$nama_penyetor = $this->input->post('nama_penyetor');
		$no_identitas = $this->input->post('no_identitas');
		$alamat = $this->input->post('alamat');
		$jenis_id = $this->input->post('jenis_id');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$kas_id = $this->input->post('kas_id');
		$username = $this->session->userdata('name');
	
		$data = array(
			'update_data' => $update_data,
			'tgl_transaksi' => $tgl_transaksi,
			'nama_penyetor' => $nama_penyetor,
			'no_identitas' => $no_identitas,
			'alamat' => $alamat,
			'jenis_id' => $jenis_id,
			'jumlah' => str_replace(',', '', $jumlah),
			'keterangan' => $keterangan,
			'kas_id' => $kas_id,
			'user_name' => $username
		);
	
		$where =  $id;
		$this->m_koperasi->update_penarikan_tunai($where,$data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil ubah Data Penarikan Tunai ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Simpanan/penarikan_tunai'));
	}
	
	public function create_penarikan_tunai(){
		$data['data1'] = $this->m_koperasi->get_tbl_anggota()->result();
		$data['data2'] = $this->m_koperasi->get_tbl_jns_simpanan()->result();
		$data['data3'] = $this->m_koperasi->get_tbl_kas()->result();
		$data['title'] = 'Koperasi';
		$data['sidebar'] = $this->load->view('layouts/sidebar','',true);
        $data['pages'] = $this->load->view('pages/Simpanan/v_create_penarikan_tunai',$data,true);
		$this->load->view('master',array('main'=>$data));
	}

	public function create_penarikan_tunai_action(){
		$tgl_transaksi = $this->input->post('tgl_transaksi');
		$nama_penyetor = $this->input->post('nama_penyetor');
		$no_identitas = $this->input->post('no_identitas');
		$alamat = $this->input->post('alamat');
		$anggota_id = $this->input->post('anggota_id');
		$jenis_id = $this->input->post('jenis_id');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$kas_id = $this->input->post('kas_id');
		$username = $this->session->userdata('name');
	
		$data = array(
			'tgl_transaksi' => $tgl_transaksi,
			'nama_penyetor' => $nama_penyetor,
			'no_identitas' => $no_identitas,
			'alamat' => $alamat,
			'anggota_id' => $anggota_id,
			'jenis_id' => $jenis_id,
			'jumlah' => $jumlah,
			'keterangan' => $keterangan,
			'akun' => 'Penarikan',
			'dk' => 'K',
			'kas_id' => $kas_id,
			'user_name' => $username
		);
	
		$this->m_koperasi->create_penarikan_tunai($data);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil tambah Data Penarikan Tunai ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Simpanan/penarikan_tunai'));
	}

	function delete_penarikan_tunai($id){
		$where = $id;
		$this->m_koperasi->delete_penarikan_tunai($where);
		$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Berhasil hapus Data Penarikan Tunai ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
		redirect(base_url('Simpanan/penarikan_tunai'));
	}
	
	
}
