<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->load->model('m_koperasi');
	}

	public function index()	{
		$data['title'] = 'Koperasi';
        $data['pages'] = $this->load->view('pages/v_login','',true);
		$this->load->view('main_login',array('main'=>$data));
	}

	// login
	function ceklogin(){
		if (isset($_POST['login'])) {
			$user=$this->input->post('username',true);
			$pass=md5($this->input->post('password',true));
			$cek=$this->m_koperasi->proseslogin($user, $pass);
			$hasil=count($cek);
			if ($hasil > 0) {
				$yglogin=$this->db->get_where('tbl_user',array('u_name'=>$user, 'pass_word'=>$pass))->row();
				$data = array('udhmasuk' => true,
					'id'=>$yglogin->id,
					'name' => $yglogin->u_name,
					'level' => $yglogin->level
				);
				$this->session->set_userdata($data);
				if ($this->session->userdata('level')=='anggota') {
					$user = $this->db->get_where('tbl_user',array('u_name'=>$user, 'pass_word'=>$pass))->row();
					$id = $user->id;
					$anggota = $this->db->get_where('tbl_anggota',array('user_id'=>$id))->row();
					$data = array('id_anggota' => $anggota->id);
					$this->session->set_userdata($data);
					// $id_anggota = $this->session->userdata('id_anggota');
					redirect('Anggota');
				}
				else{
				redirect('home');
				}
			}
			else {
				$this->session->set_flashdata('notif','<div class="alert alert-success alert-dismissible"><strong> Maaf Email atau Password Anda Salah ! </strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
				redirect('.');
			}
		}
	}

	// logout
	function keluar(){
		$this->session->sess_destroy();
		redirect('.');
	}
}
