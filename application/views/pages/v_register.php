<div class="register-box">
  <div class="register-logo">
	<h3>Register</h3>
  </div>
  <div class="register-box-body">
    <?php echo form_open_multipart('register/create/');?>
    <form>
      	<div class="form-group">
			<Label>Nama Lengkap</Label>
			<input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" required>
		</div>
		<div class="form-group">
			<Label>Username</Label>
			<input type="text" class="form-control" name="identitas" placeholder="Username" required>
		</div>
		<div class="form-group">
			<label>Jenis Kelamin</label>
			<div class="form-check form-check-inline">
				<input type="radio" class="form-check-input" name="jk" value="L">
				<label class="form-check-label">Laki-laki</label>
			</div>
			<div class="form-check form-check-inline">
				<input type="radio" class="form-check-input" name="jk" value="P">
				<label class="form-check-label">Perempuan</label>
			</div>
		</div>
		<div class="form-group">
			<Label>Alamat</Label>
			<textarea type="text" class="form-control" name="alamat" placeholder="Alamat" required></textarea>
		</div>
		<div class="form-group">
			<Label>Kota</Label>
			<input type="text" class="form-control" name="kota" placeholder="Kota" required>
		</div>
		<div class="form-group">
			<Label>Jabatan</Label>
			<select name="jabatan" class="form-control" required>
				<option value="">-- Pilih --</option>
				<option value="Pengurus">Pengurus</option>
				<option value="Anggota">Anggota</option>
			</select>
		</div>
		<div class="form-group">
			<Label>Departemen</Label>
			<select name="departement" class="form-control" required>
				<option value="">-- Pilih --</option>
				<option value="Produksi BOPP">Produksi BOPP</option>
				<option value="Produksi Slitting">Produksi Slitting</option>
				<option value="WH">WH</option>
				<option value="QA">QA</option>
				<option value="HRD">HRD</option>
				<option value="GA">GA</option>
				<option value="Purchasing">Purchasing</option>
				<option value="Accounting">Accounting</option>
				<option value="Engineering">Engineering</option>
			</select>
		</div>
		<div class="form-group">
			<Label>Aktif Keanggotaan</Label>
			<select name="aktif" class="form-control" required>
				<option value="">-- Pilih --</option>
				<option value="Y">Aktif</option>
				<option value="N">Non Aktif</option>
			</select>
		</div>
		<div class="form-group">
			<Label>Tempat Lahir</Label>
			<input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" required>
		</div>
		<div class="form-group">
			<Label>Tanggal Lahir</Label>
			<input type="date" class="form-control" name="tgl_lahir" placeholder="Tanggal Lahir" required>
		</div>
		<div class="form-group">
			<Label>Status</Label>
			<select name="status" class="form-control" required>
				<option value="">-- Pilih --</option>
				<option value="Belum Kawin">Belum Kawin</option>
				<option value="Kawin">Kawin</option>
				<option value="Cerai Hidup">Cerai Hidup</option>
				<option value="Cerai Mati">Cerai Mati</option>
				<option value="Lainnya">Lainnya</option>
			</select>
		</div>
		<div class="form-group">
			<Label>Agama</Label>
			<select name="agama" class="form-control" required>
				<option value="">-- Pilih --</option>
				<option value="Islam">Islam</option>
				<option value="Katolik">Katolik</option>
				<option value="Protestan">Protestan</option>
				<option value="Hindhu">Hindhu</option>
				<option value="Buddha">Buddha</option>
				<option value="Konghucu">Konghucu</option>
			</select>
		</div>
		<div class="form-group">
			<Label>Pekerjaan</Label>
			<select name="pekerjaan" class="form-control" required>
				<option value="">-- Pilih --</option>
				<?php foreach($main['data1'] as $d){ ?>
					<option value="<?php echo $d->jenis_kerja ?>"><?php echo $d->jenis_kerja ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="form-group">
			<Label>No Telepon</Label>
			<input type="text" class="form-control" name="notelp" placeholder="Nomor Telepon" required>
		</div>
		<div class="form-group">
			<Label>Password</Label>
			<input type="password" class="form-control" name="pass_word" placeholder="Password" required>
		</div>
		<div class="form-group">
			<Label>Foto Profil</Label>
			<input type="file" class="form-control-file" name="file_pic" required>
		</div>
		<p class="text-right">Sudah punya akun? <a href=".">Login disini!</a></p>
		<div class="row">
			<div class="col-xs-8"></div>
			<div class="col-xs-4">
				<button type="submit" name="login" class="btn btn-success btn-block btn-flat">Register</button>
			</div>
		</div>
    </form>
    <?php echo form_close()?>
  </div>
</div>
