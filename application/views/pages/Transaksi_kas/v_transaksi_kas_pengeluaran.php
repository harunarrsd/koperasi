<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Transaksi
    </h1>
    <h5 class="inline text-muted">
      Pengeluaran Kas Tunai
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Pengeluaran Kas Tunai</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Data Transaksi Pengeluaran kas</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="create_transaksi_kas_pengeluaran" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode Transaksi</th>
								<th>Tanggal Transaksi</th>
								<th>Uraian</th>
								<th>Dari Kas</th>
								<th>Untuk Akun</th>
								<th>Jumlah</th>
								<th>User</th>
								<th>Pilihan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $d->id_kas ?></td>
								<td><?php echo date("d-m-Y", strtotime($d->tgl)) ?></td>
								<td><?php echo $d->ket ?></td>
								<td><?php echo $d->dari_kas ?></td>
								<td><?php echo $d->untuk_akun ?></td>
								<td><?php echo $d->jml ?></td>
								<td><?php echo $d->user ?></td>
								<td>
									<a href="edit_transaksi_kas_pengeluaran/<?php echo $d->id_kas ?>" class="btn btn-success" title="Ubah Data"><i class="glyphicon glyphicon-edit"></i></a>
									<a href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>TransaksiKas/delete_transaksi_kas_pengeluaran/<?php echo $d->id_kas ?>';}" class="btn btn-danger" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</section>
