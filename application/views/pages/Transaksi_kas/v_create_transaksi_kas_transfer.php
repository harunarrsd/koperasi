<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Transaksi
    </h1>
    <h5 class="inline text-muted">
	  Transfer Kas
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("TransaksiKas/transaksi_kas_transfer")?>"> Transfer Kas</a></li>
      <li class="active">Buat Data Transfer Kas</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'TransaksiKas/create_transaksi_kas_transfer_action'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Tanggal Transaksi</Label>
							<input type="text" class="form-control" name="tgl_catat" id="datepicker">
						</div>
						<div class="form-group">
							<Label>Jumlah</Label>
							<input type="text" class="form-control" name="jumlah">
						</div>
						<div class="form-group">
							<Label>Keterangan</Label>
							<input type="text" class="form-control" name="keterangan">
						</div>
						<div class="form-group">
							<Label>Ambil Dari Kas</Label>
							<select name="dari_kas_id" class="form-control">
							<?php foreach($data2 as $d){ ?>
								<option value="<?php echo $d->id ?>"><?php echo $d->nama ?></option>';
							<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<Label>Transfer Ke Kas</Label>
							<select name="untuk_kas_id" class="form-control">
							<?php foreach($data2 as $d){ ?>
								<option value="<?php echo $d->id ?>"><?php echo $d->nama ?></option>';
							<?php } ?>
							</select>
						</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('TransaksiKas/transaksi_kas_transfer');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
