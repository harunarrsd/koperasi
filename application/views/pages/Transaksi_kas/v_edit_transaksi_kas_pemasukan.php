<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Transaksi
    </h1>
    <h5 class="inline text-muted">
	  Pemasukan Kas Tunai
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("TransaksiKas/transaksi_kas_pemasukan")?>"> Pemasukan Kas Tunai</a></li>
      <li class="active">Edit Data Pemasukan Kas</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'TransaksiKas/update_transaksi_kas_pemasukan'; ?>" method="post">
					<div class="box-body">
							<div class="form-group">
								<Label>Tanggal Transaksi</Label>
								<input type="hidden" class="form-control" name="id" value="<?php echo $data[0]->id ?>">
								<input type="text" class="form-control" name="tgl_catat" id="datepicker" value="<?php echo $data[0]->tgl_catat ?>">
							</div>
							<div class="form-group">
								<Label>Jumlah</Label>
								<input type="text" class="form-control" name="jumlah" value="<?php echo $data[0]->jumlah ?>">
							</div>
							<div class="form-group">
								<Label>Keterangan</Label>
								<input type="text" class="form-control" name="keterangan" value="<?php echo $data[0]->keterangan ?>">
							</div>
							<div class="form-group">
								<Label>Dari Akun</Label>
								<select name="jns_trans" class="form-control">
								<?php foreach($data1 as $d){ ?>
									<option value="<?php echo $d->id ?>" <?php if($d->id == $data[0]->jns_trans){ echo 'selected="selected"';} ?>><?php echo $d->jns_trans ?></option>';
								<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<Label>Untuk Kas</Label>
								<select name="untuk_kas_id" class="form-control">
								<?php foreach($data2 as $d){ ?>
									<option value="<?php echo $d->id ?>" <?php if($d->id == $data[0]->untuk_kas_id){ echo 'selected="selected"';} ?>><?php echo $d->nama ?></option>';
								<?php } ?>
								</select>
							</div>
						</div>
						<div class="box-footer">
							<a href="<?php echo site_url('TransaksiKas/transaksi_kas_pemasukan');?>" class="btn btn-danger">Kembali</a>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
				</form>
			</div>
		</div>
	</div>
</section>
