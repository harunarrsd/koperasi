<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Laporan
    </h1>
    <h5 class="inline text-muted">
      Buku Besar
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Laporan Buku Besar</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Laporan Buku Besar</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="cetak_buku_besar" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Cetak Laporan</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped">
						<thead class="bg-gray">
							<tr>
								<th>No.</th>
								<th>Tanggal Transaksi</th>
								<th>Jenis Transaksi</th>
								<th>Keterangan</th>
								<th>Debet</th>
								<th>Kredit</th>
								<th>Saldo</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $d->tgl?></td>
								<td><?php echo $d->transaksi?></td>
								<td><?php echo $d->ket?></td>
								<td><?php echo $d->debet?></td>
								<td><?php echo $d->kredit?></td>
								<td><?php echo ($d->debet - $d->kredit)?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
