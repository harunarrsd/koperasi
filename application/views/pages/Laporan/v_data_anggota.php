<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Laporan
    </h1>
    <h5 class="inline text-muted">
      Data Anggota
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Laporan Data Anggota</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Laporan Data Anggota</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="cetak_data_anggota" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Cetak Laporan</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped">
						<thead class="bg-gray">
							<tr>
								<th>No.</th>
								<th>ID Anggota</th>
								<th>Nama Anggota</th>
								<th>Jenis Kelamin</th>
								<th>Jabatan</th>
								<th>Alamat</th>
								<th>Status</th>
								<th>Tanggal Registrasi</th>
								<th>Foto</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo 'AG' . sprintf('%05d', $d->id) . '' ?></td>
								<td><?php echo $d->nama .'<br>'. $d->tmp_lahir.', '. $d->tgl_lahir ?></td>
								<td><?php echo $d->jk ?></td>
								<td><?php echo $d->jabatan ?></td>
								<td><?php echo $d->alamat ?></td>
								<td><?php echo $d->status ?></td>
								<td><?php echo date("d-m-Y", strtotime($d->tgl_daftar)) ?></td>
								<td><?php echo $d->file_pic ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
