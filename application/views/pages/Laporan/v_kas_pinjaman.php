<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Laporan
    </h1>
    <h5 class="inline text-muted">
      Data Kas Pinjaman
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Laporan Data Pinjaman</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Laporan Pinjaman</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="cetak_data_pinjaman" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Cetak Laporan</a>
					<table width="20%" style="font-size: 11pt; margin-top: 10pt">
						<tr>
							<td>Jumlah Peminjam</td>
							<td>: </td>
							<td><?php echo $data;?> </td>
						</tr>
						<tr>
							<td>Peminjam Lunas</td>
							<td>: </td>
							<td><?php echo $data1;?> </td>
						</tr>
						<tr>
							<td>Pinjaman Belum Lunas</td>
							<td>: </td>
							<td><?php echo $data2;?> </td>
						</tr>
					</table>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped text-center">
						<thead class="bg-gray">
							<tr>
								<th>No.</th>
								<th>Keterangan</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Pokok Pinjaman</td>
								<td><?php echo number_format($data3->jml_total)?></td>
							</tr>
							<tr>
								<td>2</td>
								<td>Tagihan Pinjaman</td>
								<td><?php echo number_format($data4->jml_total) ?></td>
							</tr>
							<tr>
								<td>3</td>
								<td>Tagihan Denda</td>
								<td><?php echo number_format($data5->total_denda) ?></td>
							</tr>
							<tr>
								<td colspan="2"><strong> Jumlah Tagihan + Denda</strong></td>
								<td><strong><?php echo number_format($data4->jml_total + $data5->total_denda) ?></strong></td>
							</tr>
							<tr>
								<td>4</td>
								<td>Tagihan Sudah Dibayar</td>
								<td><?php echo number_format($data6->jml_total) ?></td>
							</tr>
							<tr class="bg-green">
								<td>5</td>
								<td>Sisa Tagihan</td>
								<td><?php echo number_format(($data4->jml_total + $data5->total_denda) - $data6->jml_total) ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
