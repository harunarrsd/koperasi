<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Laporan
    </h1>
    <h5 class="inline text-muted">
      Neraca Saldo
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Laporan Neraca Saldo</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Laporan Neraca Saldo</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="cetak_neraca_saldo" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Cetak Laporan</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped">
						<thead class="bg-gray">
							<tr>
								<th>Nama Akun</th>
								<th>Debet</th>
								<th>Kredit</th>
							</tr>
						</thead>
						<tbody>
							<?php ; 
							foreach($data as $d){ 
							 ?>
							<tr>
								<td><?php echo $d->kd_aktiva.'. '.$d->jns_trans?></td>
								<!-- <td><?php echo number_format($d->debet) ?></td>
								<td><?php echo number_format($d->kredit) ?></td>
								<td><?php echo number_format($d->debet - $d->kredit)?></td> -->
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
