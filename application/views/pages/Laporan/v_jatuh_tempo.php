<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Laporan
    </h1>
    <h5 class="inline text-muted">
      Pembayaran Kredit
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Laporan Data Pembayaran Kredit</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Laporan Jatuh Tempo Pembayaran Kredit</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="cetak_jatuh_tempo" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Cetak Laporan</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped">
						<thead class="bg-gray">
							<tr>
								<th>No.</th>
								<th>Kode Pinjam</th>
								<th>Nama Anggota</th>
								<th>Tanggal Pinjam</th>
								<th>Tanggal Tempo</th>
								<th>Lama Pinjam</th>
								<th>Jumlah Tagihan</th>
								<th>Dibayar</th>
								<th>Sisa Tagihan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo 'TPJ' . sprintf('%05d', $d->id) ?></td>
								<td><?php echo $d->nama?></td>
								<td><?php echo $d->tgl_pinjam?></td>
								<td><?php echo $d->tempo?></td>
								<td><?php echo $d->lama_angsuran?></td>
								<td><?php echo number_format($d->jum_bayar) ?></td>
								<td><?php echo number_format($d->jum_denda) ?></td>
								<td><?php echo number_format($d->jum_bayar - $d->jum_denda) ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
