<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Laporan
    </h1>
    <h5 class="inline text-muted">
      Saldo
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Laporan Data Saldo Kas</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Laporan Saldo Kas</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="cetak_saldo_kas" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Cetak Laporan</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped text-center">
						<thead class="bg-gray">
							<tr>
								<th>No.</th>
								<th>Nama Kas</th>
								<th>Saldo</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){
							$no++;?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $d->nama ?></td>
								<td><?php 'saldo' ?></td>
							</tr>
							<?php } ?>
								<tr class="bg-gray">
									<td colspan="2"><strong>Jumlah</strong></td>
									<td><?php echo 'number_format(($data4->jml_total + $data5->total_denda) - $data6->jml_total)' ?></td>
								</tr>
								<tr class="bg-green">
									<td colspan="2"><strong>Saldo</strong></td>
									<td><?php echo 'number_format(($data4->jml_total + $data5->total_denda) - $data6->jml_total)' ?></td>
								</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
