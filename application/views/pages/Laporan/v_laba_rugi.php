<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Laporan
    </h1>
    <h5 class="inline text-muted">
      Laba Rugi
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Laporan Laba Rugi</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Laporan Laba Rugi</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="cetak_saldo_kas" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Cetak Laporan</a>
				</div>
				<div class="box-body table-responsive">
					<div class="h4">Estimasi Data Pinjaman</div>
					<table class="table table-bordered table-striped text-center">
						<thead class="bg-gray">
							<tr>
								<th>No.</th>
								<th>Keterangan</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Jumlah Pinjaman</td>
								<td><?php echo number_format( $data->jml_total) ?></td>
							</tr>
							<tr>
								<td>2</td>
								<td>Pendapatan Biaya Administrasi</td>
								<td><?php echo number_format( $data1->jml_total) ?></td>
							</tr>
							<tr>
								<td>3</td>
								<td>Pendapatan Biaya Bunga</td>
								<td><?php echo number_format( $data2->jml_total) ?></td>
							</tr>
							<tr>
								<td>4</td>
								<td>Pendapatan Biaya Pembulatan</td>
								<td><?php echo number_format( $data3->jml_total - ($data->jml_total + $data1->jml_total + $data2->jml_total)) ?></td>
							</tr>
								<tr class="bg-gray">
									<td colspan="2"><strong>Jumlah Tagihan</strong></td>
									<td><?php echo number_format($data3->jml_total) ?></td>
								</tr>
								<tr class="bg-gray">
									<td colspan="2"><strong>Estimasi Pendapatan Pinjaman</strong></td>
									<td><?php echo number_format($data3->jml_total - $data->jml_total) ?></td>
								</tr>
						</tbody>
					</table><br>
					<div class="h4">Pendapatan</div>
					<table class="table table-bordered table-striped text-center">
						<thead class="bg-gray">
							<tr>
								<th>No.</th>
								<th>Keterangan</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Pendapatan Pinjaman</td>
								<td><?php echo number_format($data4->jml_total - $data->jml_total) ?></td>
							</tr>
							<tr>
								<td>2</td>
								<td>Pendapatan Lainnya</td>
								<td></td>
							</tr>
							<tr class="bg-gray">
								<td colspan="2"><strong>Jumlah Pendapatan</strong></td>
								<td><?php echo 'number_format(($data4->jml_total + $data5->total_denda) - $data6->jml_total)' ?></td>
							</tr>
						</tbody>
					</table><br>
					<div class="h4">Biaya-biaya</div>
					<table class="table table-bordered table-striped text-center">
						<thead class="bg-gray">
							<tr>
								<th>No.</th>
								<th>Keterangan</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Beban Gaji Karyawan</td>
								<td></td>
							</tr>
							<tr>
								<td>2</td>
								<td>Biaya Listrik dan Air</td>
								<td></td>
							</tr>
							<tr>
								<td>3</td>
								<td>Biaya Transportasi</td>
								<td></td>
							</tr>
							<tr>
								<td>3</td>
								<td>Biaya Lainnya</td>
								<td></td>
							</tr>
							<tr class="bg-gray">
								<td colspan="2"><strong>Jumlah Biaya</strong></td>
								<td><?php echo 'number_format(($data4->jml_total + $data5->total_denda) - $data6->jml_total)' ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
