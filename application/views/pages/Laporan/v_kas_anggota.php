<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Laporan
    </h1>
    <h5 class="inline text-muted">
      Data Kas Anggota
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Laporan Data Kas Anggota</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Laporan Data Kas per Anggota</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="cetak_data_anggota" class="btn btn-default"><i class="glyphicon glyphicon-print"></i> Cetak Laporan</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped">
						<thead class="bg-gray">
							<tr>
								<th>No.</th>
								<th>Foto</th>
								<th>Identitas</th>
								<th>Saldo Simpanan</th>
								<th>Tagihan Kredit</th>
								<th>Keterangan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
								foreach($data as $d){ 
								$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $d->file_pic ?></td>
								<td><?php echo 'ID Anggota: '.'AG' . sprintf('%05d', $d->id).'<br>'.
												'Nama: '.$d->nama.'<br>'.
												'Jenis Kelamin: '.$d->jk.'<br>'.
												'Alamat: '.$d->alamat?></td>
								<td><?php echo $d->jml_setor?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
