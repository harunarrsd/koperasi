<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Pinjaman
    </h1>
    <h5 class="inline text-muted">
      Pelunasan Pinjaman
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Pelunasan Pinjaman</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Data Pinjaman</h4>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped dataTable">
						<thead class="bg-primary">
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Nama Anggota</th>
								<th>Dept</th>
								<th>Tanggal Pinjam</th>
								<th>Tanggal Tempo</th>
								<th>Lama Pinjam</th>
								<th>Total Pinjaman</th>
								<th>Total Denda</th>
								<th>Dibayar</th>
								<th>Pilihan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo 'TPJ' . sprintf('%05d', $d->id) ?></td>
								<td><?php echo $d->nama_anggota ?></td>
								<td><?php echo $d->departement ?></td>
								<td><?php echo date("d M Y", strtotime($d->tgl_pinjam)) ?></td>
								<td><?php echo date("d M Y", strtotime($d->tempo)) ?></td>
								<td><?php echo number_format($d->lama_angsuran) ?></td>
								<td><?php echo number_format($d->total_tagihan) ?></td>
								<td><?php echo number_format($d->total_denda) ?></td>
								<td><?php echo number_format($d->total_bayar) ?></td>
								<td>
									<a href="detail_setoran_tunai/<?php echo $d->id ?>" class="btn btn-primary" title="Ubah Data"><i class="glyphicon glyphicon-search"></i></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</section>
