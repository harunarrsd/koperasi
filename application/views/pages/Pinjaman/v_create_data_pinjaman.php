<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Pinjaman
    </h1>
    <h5 class="inline text-muted">
	  Data Pinjaman
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("Pinjaman/data_pinjaman")?>"> Penarikan Tunai</a></li>
      <li class="active">Buat Data Penarikan Tunai</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'Pinjaman/create_data_pinjaman_action'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Tanggal Pinjam</Label>
							<input type="text" class="form-control" name="tgl_pinjam" id="datepicker">
							</div>
							<div class="form-group">
								<Label>Nama Anggota</Label>
								<select name="anggota_id" class="form-control">
									<?php foreach($data1 as $d){ ?>
									<option value="<?php echo $d->id ?>"><?php echo $d->nama ?></option>';
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<Label>Nama Barang</Label>
								<select name="barang_id" class="form-control">
									<?php foreach($data2 as $d){ ?>
									<option value="<?php echo $d->id ?>"><?php echo $d->nm_barang .' Rp.'. $d->harga ?></option>';
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<Label>Harga Barang</Label>
								<input type="text" class="form-control" name="jumlah" >
							</div>
							<div class="form-group">
								<Label>Lama Angsuran</Label>
								<select name="lama_angsuran" class="form-control">
									<?php foreach($data3 as $d){ ?>
									<option value="<?php echo $d->ket ?>"><?php echo $d->ket ?></option>';
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<Label>Bunga</Label>
								<input type="text" class="form-control" name="bunga">
							</div>
							<div class="form-group">
								<Label>Biaya Administrasi</Label>
								<input type="text" class="form-control" name="biaya_adm">
							</div>
							<div class="form-group">
								<Label>Ambil dari Kas</Label>
								<select name="kas_id" class="form-control">
									<?php foreach($data4 as $d){ ?>
									<option value="<?php echo $d->id ?>"><?php echo $d->nama ?></option>';
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<Label>Keterangan</Label>
								<textarea class="form-control" name="keterangan"></textarea>
							</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('Pinjaman/data_pinjaman');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
