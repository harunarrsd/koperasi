<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Pinjaman
    </h1>
    <h5 class="inline text-muted">
      Pengajuan
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Pengajuan Pinjaman</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4 class="text-center">Data Pengajuan</h4>
					<?php echo $this->session->flashdata('notif')?>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped dataTable text-center">
						<thead class="bg-primary">
							<tr>
								<th>No.</th>
								<th>ID Anggota</th>
								<th>Tanggal Pengajuan</th>
								<th>Anggota</th>
								<th>Jenis</th>
								<th>Jumlah</th>
								<th>Jml Angsuran</th>
								<th>Keterangan</th>
								<th>Tanggal Update</th>
								<th>Status</th>
								<th>Sisa Pinjaman</th>
								<th>Pilihan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $d->ajuan_id ?></td>
								<td><?php echo date("d M Y", strtotime($d->tgl_input)) ?></td>
								<td><?php echo $d->anggota_id .'<br>'.'<strong>'.$d->nama .'</strong>'.'<br>'. $d->departement ?></td>
								<td><?php echo $d->jenis ?></td>
								<td><?php echo number_format($d->nominal) ?></td>
								<td><?php echo $d->lama_ags.' bln' ?></td>
								<td><?php echo $d->keterangan ?></td>
								<td><?php echo date("d M Y", strtotime($d->tgl_update)) ?></td>
								<td><?php echo $d->status ?></td>
								<td><?php echo $d->status ?></td>
								<td>
									<a href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>';}" class="btn btn-success" title="Setujui"><i class="glyphicon glyphicon-check"></i></a>
									<a href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>';}" class="btn btn-warning" title="Tolak"><i class="glyphicon glyphicon-warning-sign"></i></a>
									<a href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>';}" class="btn btn-primary" title="Pending"><i class="glyphicon glyphicon-question-sign"></i></a>
									<a href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>';}" class="btn button-a-background bg-purple" title="Cetak"><i class="glyphicon glyphicon-print"></i></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</section>
