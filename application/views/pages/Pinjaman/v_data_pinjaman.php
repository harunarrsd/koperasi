<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Pinjaman
    </h1>
    <h5 class="inline text-muted">
      Data Pinjaman
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Data Pinjaman</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Pinjaman -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Data Pinjaman Anggota</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="create_data_pinjaman" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped dataTable">
						<thead class="bg-primary">
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Tanggal Pinjam</th>
								<th>Nama Anggota</th>
								<th>Hitungan</th>
								<th>Total Tagihan</th>
								<th>Lunas</th>
								<th>User</th>
								<th>Pilihan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo 'PJ' . sprintf('%05d', $d->id) . '' ?></td>
								<td><?php echo date("d-m-Y", strtotime($d->tgl_pinjam)) ?></td>
								<td><?php echo $d->identitas .'<br>'.'<strong>'.$d->nama .'</strong>'.'<br>'. $d->departement ?></td>
								<td><a data-toggle="modal" data-target="#tampil_hitungan<?php echo $d->id ?>" href="" class="btn btn-default" title="Lihat Detail Data"><i class="glyphicon glyphicon-eye-open"></i></a></td>
								<td><a data-toggle="modal" data-target="#tampil_total<?php echo $d->id ?>" href="" class="btn btn-default" title="Lihat Detail Data"><i class="glyphicon glyphicon-eye-open"></i></a></td>
								<td><?php echo $d->lunas ?></td>
								<td><?php echo $d->username ?></td>
								<td>
									<a href="detail_data_pinjaman/<?php echo $d->id ?>" class="btn btn-primary" title="Detail Data"><i class="glyphicon glyphicon-search"></i></a>
									<a href="edit_data_pinjaman/<?php echo $d->id ?>" class="btn btn-success" title="Ubah Data"><i class="glyphicon glyphicon-edit"></i></a>
									<a href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>Pinjaman/delete_data_pinjaman/<?php echo $d->id ?>';}" class="btn btn-danger" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
					<!-- Modal -->
					<?php foreach($data as $d){ ?>
					<div class="modal fade" id="tampil_hitungan<?php echo $d->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h3 class="modal-title">Detail Hitungan</h3>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form>
										<div class="form-group">
											<label >Nama Barang:</label>
											<input type="text" class="form-control" value="<?php echo $d->nm_barang ?>" disabled >
										</div>
										<div class="form-group">
											<label >Harga Barang:</label>
											<input type="text" class="form-control" value="<?php echo $d->jumlah ?>" disabled >
										</div>
										<div class="form-group">
											<label >Lama Angsuran:</label>
											<input type="text" class="form-control" value="<?php echo $d->lama_angsuran ?>" disabled >
										</div>
										<div class="form-group">
											<label >Pokok Angsuran:</label>
											<input type="text" class="form-control" value="<?php echo number_format($d->pokok_angsuran) ?>" disabled >
										</div>
										<div class="form-group">
											<label >Bunga Pinjaman:</label>
											<input type="text" class="form-control" value="<?php echo $d->bunga_pinjaman ?>" disabled >
										</div>
										<div class="form-group">
											<label >Biaya Admin:</label>
											<input type="text" class="form-control" value="<?php echo number_format($d->biaya_adm) ?>" disabled >
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
								</div>
							</div>
						</div>
					</div>
					<div class="modal fade" id="tampil_total<?php echo $d->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h3 class="modal-title">Detail Total Tagihan</h3>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form>
										<div class="form-group">
											<label >Jumlah Angsuran:</label>
											<input type="text" class="form-control" value="<?php echo $d->ags_per_bulan ?>" disabled >
										</div>
										<div class="form-group">
											<label >Jumlah Denda:</label>
											<input type="text" class="form-control" value="<?php echo $d->jumlah ?>" disabled >
										</div>
										<div class="form-group">
											<label >Total Tagihan:</label>
											<input type="text" class="form-control" value="<?php echo $d->jumlah ?>" disabled >
										</div>
										<div class="form-group">
											<label >Sudah Dibayar:</label>
											<input type="text" class="form-control" value="<?php echo $d->jumlah ?>" disabled >
										</div>
										<div class="form-group">
											<label >Sisa Angsuran:</label>
											<input type="text" class="form-control" value="<?php echo $d->jumlah ?>" disabled >
										</div>
										<div class="form-group">
											<label >Sisa Angsuran:</label>
											<input type="text" class="form-control" value="<?php echo $d->jumlah ?>" disabled >
										</div>
									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
</section>
