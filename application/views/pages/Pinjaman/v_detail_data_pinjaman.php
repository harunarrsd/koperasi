<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Detail Pinjaman
    </h1>
    <h5 class="inline text-muted">
	  Kode Pinjaman <?php echo 'PJ' . sprintf('%05d', $data[0]->id) ?>
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("Pinjaman/data_pinjaman")?>"> Pinjaman</a></li>
      <li class="active"> Detail Data Pinjaman</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header">
					<?php echo $this->session->flashdata('notif')?>
					<a href="<?php echo site_url('Pinjaman/data_pinjaman');?>" class="btn btn-danger"><i class="glyphicon-arrow-left"></i> Kembali</a>
					<a href="<?php echo site_url('Pinjaman/data_pinjaman');?>" class="btn btn-success"><i class="glyphicon-print"></i>Cetak Detail</a>
					<!-- Content Header -->
					<div class="row">
						<div class="col-xs-12">
								<div class="col-xs-1">
									<!-- Buat Foto gambar -->
									<?php echo $data[0]->file_pic ?>
								</div>
								<div class="col-xs-2">
									<div class="row">
										<h5><strong>Data Anggota</strong></h5>
									</div>
									<div class="row">
										<p>ID Anggota</p>
										<p>Nama Anggota</p>
										<p>Dept</p>
										<p>Tempat, Tanggal Lahir</p>
										<p>Kota Tinggal</p>
									</div>
								</div>
								<div class="col-xs-2">
									<div class="row">
										<h5><strong>tes</strong></h5>
									</div>
									<div class="row">
										<p><?php echo 'AG' . sprintf('%05d', $data[0]->id_anggota) ?></p>
										<p><?php echo $data[0]->nama ?></p>
										<p><?php echo $data[0]->departement ?></p>
										<p><?php echo $data[0]->tmp_lahir.', '.$data[0]->tgl_lahir ?></p>
										<p><?php echo $data[0]->kota ?></p>
									</div>
								</div>
								<div class="col-xs-2">
									<div class="row">
										<h5><strong>Data Pinjaman </strong></h5>
									</div>
									<div class="row">
										<p>Kode Pinjam</p>
										<p>Tanggal Pinjam</p>
										<p>Tanggal Tempo</p>
										<p>Lama Pinjaman</p>
									</div>
								</div>
								<div class="col-xs-2">
									<div class="row">
										<h5><strong>tes </strong></h5>
									</div>
									<div class="row">
										<p><?php echo 'PJ' . sprintf('%05d', $data[0]->id) ?></p>
										<p><?php echo date("d-m-Y", strtotime($data[0]->tgl_pinjam)) ?></p>
										<p><?php echo date("d-m-Y", strtotime($data[0]->tempo)) ?></p>
										<p><?php echo $data[0]->lama_angsuran ?></p>
									</div>
								</div>
								<div class="col-xs-2">
									<div class="row">
										<h5><strong>Data Pinjaman </strong></h5>
									</div>
									<div class="row">
										<p>Pokok Pinjamam</p>
										<p>Angsuran Pokok</p>
										<p>Biaya dan Bunga</p>
										<p>Jumlah Angsuran</p>
									</div>
								</div>
								<div class="col-xs-1">
									<div class="row">
										<h5><strong>tes </strong></h5>
									</div>
									<div class="row">
										<p><?php echo number_format($data[0]->jumlah) ?></p>
										<p><?php echo number_format($data[0]->pokok_angsuran) ?></p>
										<p><?php echo number_format(($data[0]->biaya_adm+$data[0]->bunga_pinjaman)) ?></p>
										<p><?php echo number_format($data[0]->ags_per_bulan) ?></p>
									</div>
								</div>
						</div>
					</div>
				</div>
				<div class="box-body">
					<h4>Data Transaksi Penarikan Tunai</h4>
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-bordered table-striped text-center">
								<thead class="bg-primary">
									<th>No.</th>
									<th>Kode Bayar</th>
									<th>Tanggal Bayar</th>
									<th>Angsuran Ke-</th>
									<th>Jenis Pembayaran</th>
									<th>Jumlah Bayar</th>
									<th>Denda</th>
									<th>User</th>
								</thead>
								<tbody>
								<?php $no=0; 
								foreach($data1 as $d){ 
								$no++; ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><?php echo 'TBY' . sprintf('%05d', $d->id) ?></td>
										<td><?php echo date("d-m-Y", strtotime($d->tgl_bayar)) ?></td>
										<td><?php echo $d->angsuran_ke ?></td>
										<td><?php echo $d->angsuran ?></td>
										<td><?php echo $d->jumlah_bayar ?></td>
										<td><?php echo $d->denda ?></td>
										<td><?php echo $d->username ?></td>
									</tr>
								<?php } ?>
									<tr>
										<td colspan="5"><strong>Jumlah</strong></td>
										<td><strong><?php echo $d->total_jml_bayar ?></strong></td>
										<td><strong><?php echo $d->total_denda ?></strong></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<h4>Data Pembayaran Angsuran</h4>
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-bordered table-striped text-center">
								<thead class="bg-primary">
									<th>No.</th>
									<th>Kode Bayar</th>
									<th>Tanggal Bayar</th>
									<th>Angsuran Ke-</th>
									<th>Jenis Pembayaran</th>
									<th>Jumlah Bayar</th>
									<th>Denda</th>
									<th>User</th>
								</thead>
								<tbody>
								<?php $no=0; 
								foreach($data1 as $d){ 
								$no++; ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><?php echo 'TBY' . sprintf('%05d', $d->id) ?></td>
										<td><?php echo date("d-m-Y", strtotime($d->tgl_bayar)) ?></td>
										<td><?php echo $d->angsuran_ke ?></td>
										<td><?php echo $d->angsuran ?></td>
										<td><?php echo $d->jumlah_bayar ?></td>
										<td><?php echo $d->denda ?></td>
										<td><?php echo $d->username ?></td>
									</tr>
								<?php } ?>
									<tr>
										<td colspan="5"><strong>Jumlah</strong></td>
										<td><strong><?php echo $d->total_jml_bayar ?></strong></td>
										<td><strong><?php echo $d->total_denda ?></strong></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
