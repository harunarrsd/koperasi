<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Pinjaman
    </h1>
    <h5 class="inline text-muted">
	  Data Pinjaman
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("Pinjaman/data_pinjaman")?>"> Pinjaman</a></li>
      <li class="active">Edit Data Pinjaman</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'Pinjaman/update_data_pinjaman'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Tanggal Pinjam</Label>
							<input type="hidden" class="form-control" name="id" value="<?php echo $data[0]->id?>">
							<input type="text" class="form-control" name="tgl_pinjam" id="datepicker" value="<?php echo $data[0]->tgl_pinjam ?>">
							</div>
							<div class="form-group">
								<Label>Lama Angsuran</Label>
								<select name="lama_angsuran" class="form-control">
									<?php foreach($data3 as $d){ ?>
									<option value="<?php echo $d->ket ?>" <?php if($d->ket == $data[0]->lama_angsuran){ echo 'selected="selected"';} ?>><?php echo $d->ket ?></option>';
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<Label>Harga Barang</Label>
								<input type="text" class="form-control" name="jumlah" value="<?php echo $data[0]->jumlah ?>">
							</div>
							<div class="form-group">
								<Label>Bunga</Label>
								<input type="text" class="form-control" name="bunga" value="<?php echo $data[0]->bunga ?>">
							</div>
							<div class="form-group">
								<Label>Biaya Administrasi</Label>
								<input type="text" class="form-control" name="biaya_adm" value="<?php echo $data[0]->biaya_adm ?>">
							</div>
							<div class="form-group">
								<Label>Ambil dari Kas</Label>
								<select name="kas_id" class="form-control">
									<?php foreach($data4 as $d){ ?>
									<option value="<?php echo $d->id ?>" <?php if($d->id == $data[0]->kas_id){ echo 'selected="selected"';} ?>><?php echo $d->nama ?></option>';
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<Label>Keterangan</Label>
								<textarea class="form-control" name="keterangan" ><?php echo $data[0]->keterangan ?></textarea>
							</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('Pinjaman/data_pinjaman');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
