<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Data Barang
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Data Barang</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Data Barang</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="create_data_barang" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah Data Barang</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Barang</th>
								<th>Type</th>
								<th>Merk</th>
								<th>Harga (Rp.)</th>
								<th>Jumlah Barang</th>
								<th>Ket</th>
								<th>Pilihan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $d->nm_barang ?></td>
								<td><?php echo $d->type ?></td>
								<td><?php echo $d->merk ?></td>
								<td><?php echo $d->harga ?></td>
								<td><?php echo $d->jml_brg ?></td>
								<td><?php echo $d->ket ?></td>
								<td>
									<a href="edit_data_barang/<?php echo $d->id ?>" class="btn btn-success" title="Ubah Data"><i class="glyphicon glyphicon-edit"></i></a>
									<a href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>MasterData/delete_data_barang/<?php echo $d->id ?>';}" class="btn btn-danger" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</section>
