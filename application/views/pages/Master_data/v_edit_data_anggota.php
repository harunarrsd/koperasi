<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Data Anggota
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("MasterData/data_anggota")?>">Data Anggota</a></li>
      <li class="active">Buat Data Anggota</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'MasterData/update_data_anggota'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Nama Lengkap</Label>
							<input type="hidden" class="form-control" name="id" value="<?php echo $data[0]->id ?>">
							<input type="text" class="form-control" name="nama" value="<?php echo $data[0]->nama ?>">
						</div>
						<div class="form-group">
							<Label>Username</Label>
							<input type="text" class="form-control" name="identitas" value="<?php echo $data[0]->identitas ?>">
						</div>
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<div class="form-check form-check-inline">
								<input type="radio" class="form-check-input" name="jk" <?php if($data[0]->jk == "L"){ echo 'checked="checked"'; } ?>  value="L">
								<label class="form-check-label">Laki-laki</label>
							</div>
							<div class="form-check form-check-inline">
								<input type="radio" class="form-check-input" name="jk" <?php if($data[0]->jk == "P"){ echo 'checked="checked"'; } ?>  value="P">
								<label class="form-check-label">Perempuan</label>
							</div>
						</div>
						<div class="form-group">
							<Label>Alamat</Label>
							<textarea type="text" class="form-control" name="alamat"> <?php echo $data[0]->alamat?> </textarea>
						</div>
						<div class="form-group">
							<Label>Kota</Label>
							<input type="text" class="form-control" name="kota" value="<?php echo $data[0]->kota ?>">
						</div>
						<div class="form-group">
							<Label>Jabatan</Label>
							<select name="jabatan" class="form-control">
								<option <?php if($data[0]->jabatan == 'Pengurus'){ echo 'selected="selected"'; } ?>  value="Pengurus">Pengurus</option>
								<option <?php if($data[0]->jabatan == 'Anggota'){ echo 'selected="selected"'; } ?>  value="Anggota">Anggota</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Departemen</Label>
							<select name="departement" class="form-control">
								<option <?php if($data[0]->departement == 'Produksi BOPP'){ echo 'selected="selected"'; } ?> value="Produksi BOPP">Produksi BOPP</option>
								<option <?php if($data[0]->departement == 'Produksi Slitting'){ echo 'selected="selected"'; } ?> value="Produksi Slitting">Produksi Slitting</option>
								<option <?php if($data[0]->departement == 'WH'){ echo 'selected="selected"'; } ?> value="WH">WH</option>
								<option <?php if($data[0]->departement == 'QA'){ echo 'selected="selected"'; } ?> value="QA">QA</option>
								<option <?php if($data[0]->departement == 'HRD'){ echo 'selected="selected"'; } ?> value="HRD">HRD</option>
								<option <?php if($data[0]->departement == 'GA'){ echo 'selected="selected"'; } ?> value="GA">GA</option>
								<option <?php if($data[0]->departement == 'Purchasing'){ echo 'selected="selected"'; } ?> value="Purchasing">Purchasing</option>
								<option <?php if($data[0]->departement == 'Accounting'){ echo 'selected="selected"'; } ?> value="Accounting">Accounting</option>
								<option <?php if($data[0]->departement == 'Engineering'){ echo 'selected="selected"'; } ?> value="Engineering">Engineering</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Tanggal Registrasi</Label>
							<input type="text" class="form-control" id="datepicker" name="tgl_daftar" value="<?php echo $data[0]->tgl_daftar ?>">
						</div>
						<div class="form-group">
							<Label>Aktif Keanggotaan</Label>
							<select name="aktif" class="form-control">
								<option <?php if($data[0]->aktif == "Y"){ echo 'selected="selected"'; } ?> value="Y">Aktif</option>
								<option <?php if($data[0]->aktif == "N"){ echo 'selected="selected"'; } ?> value="N">Non Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Tempat Lahir</Label>
							<input type="text" class="form-control" name="tmp_lahir" value="<?php echo $data[0]->tmp_lahir ?>">
						</div>
						<div class="form-group">
							<Label>Status</Label>
							<select name="status" class="form-control">
								<option <?php if($data[0]->aktif == "Belum Kawin"){ echo 'selected="selected"'; } ?> value="Belum Kawin">Belum Kawin</option>
								<option <?php if($data[0]->aktif == "Kawin"){ echo 'selected="selected"'; } ?> value="Kawin">Kawin</option>
								<option <?php if($data[0]->aktif == "Cerai Hidup"){ echo 'selected="selected"'; } ?> value="Cerai Hidup">Cerai Hidup</option>
								<option <?php if($data[0]->aktif == "Cerai Mati"){ echo 'selected="selected"'; } ?> value="Cerai Mati">Cerai Mati</option>
								<option <?php if($data[0]->aktif == "Cerai"){ echo 'selected="selected"'; } ?> value="Cerai">Cerai</option>
								<option <?php if($data[0]->aktif == "Lainnya"){ echo 'selected="selected"'; } ?> value="Lainnya">Lainnya</option>
							</select>
						<div class="form-group">
							<Label>Agama</Label>
							<select name="agama" class="form-control">
								<option <?php if($data[0]->agama == "Islam"){ echo 'selected="selected"'; } ?> value="Islam">Islam</option>
								<option <?php if($data[0]->agama == "Katolik"){ echo 'selected="selected"'; } ?> value="Katolik">Katolik</option>
								<option <?php if($data[0]->agama == "Protestan"){ echo 'selected="selected"'; } ?> value="Protestan">Protestan</option>
								<option <?php if($data[0]->agama == "Hindhu"){ echo 'selected="selected"'; } ?> value="Hindhu">Hindhu</option>
								<option <?php if($data[0]->agama == "Budha"){ echo 'selected="selected"'; } ?> value="Buddha">Budha</option>
								<option <?php if($data[0]->agama == "Konghucu"){ echo 'selected="selected"'; } ?> value="Konghucu">Konghucu</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Pekerjaan</Label>
							<input type="text" class="form-control" name="pekerjaan" value="<?php echo $data[0]->pekerjaan ?>">
						</div>
						<div class="form-group">
							<Label>No Telepon</Label>
							<input type="text" class="form-control" name="notelp" value="<?php echo $data[0]->notelp ?>">
						</div>
						<div class="form-group">
							<Label>Password</Label>
							<input type="password" class="form-control" name="pass_word">
						</div>
						<div class="form-group">
							<Label>Foto Profil</Label>
							<input type="file" class="form-control-file" name="file_pic">
						</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('MasterData/data_anggota');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
