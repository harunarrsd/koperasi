<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Jenis Simpanan
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("MasterData/jns_simpanan")?>">Jenis Simpanan</a></li>
      <li class="active">Edit Jenis Simpanan</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'MasterData/update_jns_simpanan'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Jenis Simpanan</Label>
							<input type="hidden" class="form-control" name="id" value="<?php echo $data[0]->id ?>">
							<input type="text" class="form-control" name="jns_simpanan" value="<?php echo $data[0]->jns_simpan ?>">
						</div>
						<div class="form-group">
							<Label>Jumlah</Label>
							<input type="text" class="form-control" name="jumlah" value="<?php echo $data[0]->jumlah ?>">
						</div>
						<div class="form-group">
							<Label>Tampil</Label>
							<select name="tampil" class="form-control" placeholder="Tampil">
								<option <?php if($data[0]->tampil == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->tampil == "T"){ echo 'selected="selected"'; } ?> value="T">T</option>
							</select>
						</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('MasterData/jns_simpanan');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
