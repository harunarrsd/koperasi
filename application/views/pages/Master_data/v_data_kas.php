<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Jenis Kas
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Jenis Kas</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Jenis Kas</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="create_data_kas" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah Jenis Kas</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Kas</th>
								<th>Aktif</th>
								<th>Simpanan</th>
								<th>Penarikan</th>
								<th>Pinjaman</th>
								<th>Angsuran</th>
								<th>Pemasukan Kas</th>
								<th>Pengeluaran Kas</th>
								<th>Transfer Kas</th>
								<th>Pilihan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $d->nama ?></td>
								<td><?php echo $d->aktif ?></td>
								<td><?php echo $d->tmpl_simpan ?></td>
								<td><?php echo $d->tmpl_penarikan ?></td>
								<td><?php echo $d->tmpl_pinjaman ?></td>
								<td><?php echo $d->tmpl_bayar ?></td>
								<td><?php echo $d->tmpl_pemasukan ?></td>
								<td><?php echo $d->tmpl_pengeluaran ?></td>
								<td><?php echo $d->tmpl_transfer ?></td>
								<td>
									<a href="edit_data_kas/<?php echo $d->id ?>" class="btn btn-success" title="Ubah Data"><i class="glyphicon glyphicon-edit"></i></a>
									<a href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>MasterData/delete_data_kas/<?php echo $d->id ?>';}" class="btn btn-danger" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</section>
