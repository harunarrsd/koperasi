<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Data Anggota
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Data Anggota</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Data Anggota</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="create_data_anggota" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah Data Anggota</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr>
								<th>No.</th>
								<th>Photo</th>
								<th>ID Anggota</th>
								<th>Username</th>
								<th>Nama Lengkap</th>
								<th>Jenis Kelamin</th>
								<th>ALamat</th>
								<th>Kota</th>
								<th>Jabatan</th>
								<th>Department</th>
								<th>Tanggal Registrasi</th>
								<th>Aktif Keanggotaan</th>
								<th>Pilihan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $d->file_pic ?></td>
								<td><?php echo 'AG' . sprintf('%05d', $d->id) ?></td>
								<td><?php echo $d->identitas ?></td>
								<td><?php echo $d->nama ?></td>
								<td><?php echo $d->jk ?></td>
								<td><?php echo $d->alamat ?></td>
								<td><?php echo $d->kota ?></td>
								<td><?php echo $d->jabatan ?></td>
								<td><?php echo $d->departement ?></td>
								<td><?php echo $d->tgl_daftar ?></td>
								<td><?php echo $d->aktif ?></td>
								<td>
									<a href="edit_data_anggota/<?php echo $d->id ?>" class="btn btn-success" title="Ubah Data"><i class="glyphicon glyphicon-edit"></i></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
