<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Jenis Angsuran
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("MasterData/lama_angsuran")?>">Jenis Angsuran</a></li>
      <li class="active">Buat Jenis Angsuran</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'MasterData/create_lama_angsuran_action'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Lama Angsuran (Bulan)</Label>
							<input type="text" class="form-control" name="ket">
						</div>
						<div class="form-group">
							<Label>Aktif</Label>
							<select name="aktif" class="form-control">
								<option value="Y">Y</option>
								<option value="T">T</option>
							</select>
						</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('MasterData/lama_angsuran');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
