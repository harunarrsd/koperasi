<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Jenis Akun Transaksi
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Jenis Akun Transaksi</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Jenis Akun Transaksi</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="create_jns_akun" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah Jenis Akun Transaksi</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kd Aktiva</th>
								<th>Jenis Transaksi</th>
								<th>Akun</th>
								<th>Pemasukan</th>
								<th>Pengeluaran</th>
								<th>Aktif</th>
								<th>Laba Rugi</th>
								<th>Pilihan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
							<td><?php echo $no ?></td>
								<td><?php echo $d->kd_aktiva ?></td>
								<td><?php echo $d->jns_trans ?></td>
								<td><?php echo $d->akun ?></td>
								<td><?php echo $d->pemasukan ?></td>
								<td><?php echo $d->pengeluaran ?></td>
								<td><?php echo $d->aktif ?></td>
								<td><?php echo $d->laba_rugi ?></td>
								<td>
								<a href="edit_jns_akun/<?php echo $d->id ?>" class="btn btn-success" title="Ubah Data"><i class="glyphicon glyphicon-edit"></i></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</section>
