  <!-- Main content -->
<section class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3>Formulir Pengajuan</h3>
				</div>
				<!-- Form -->
				<form action="<?php echo base_url(). 'MasterData/create_data_pengguna_action'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Username</Label>
							<input type="text" class="form-control" name="u_name">
						</div>
						<div class="form-group">
							<Label>Level</Label>
							<select name="level" class="form-control">
								<option value="admin">Admin</option>
								<option value="operator">Operator</option>
								<option value="pinjaman">Pinjaman</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Aktif</Label>
							<select name="aktif" class="form-control">
								<option value="Y">Y</option>
								<option value="T">T</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Password</Label>
							<input type="password" class="form-control" name="pass_word">
						</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('MasterData/data_pengguna');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
