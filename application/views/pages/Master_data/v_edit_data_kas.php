<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Jenis Kas
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("MasterData/data_kas")?>">Jenis Kas</a></li>
      <li class="active">Edit Jenis Kas</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'MasterData/update_data_kas'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Nama Kas</Label>
							<input type="hidden" class="form-control" name="id" value="<?php echo $data[0]->id ?>">
							<input type="text" class="form-control" name="nama" value="<?php echo $data[0]->nama ?>">
						</div>
						<div class="form-group">
							<Label>Aktif</Label>
							<select name="aktif" class="form-control">
								<option <?php if($data[0]->aktif == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->aktif == "T"){ echo 'selected="selected"'; } ?> value="T">T</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Simpanan</Label>
							<select name="tmpl_simpan" class="form-control">
								<option <?php if($data[0]->tmpl_simpan == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->tmpl_simpan == "T"){ echo 'selected="selected"'; } ?> value="T">T</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Penarikan</Label>
							<select name="tmpl_penarikan" class="form-control">
								<option <?php if($data[0]->tmpl_penarikan == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->tmpl_penarikan == "T"){ echo 'selected="selected"'; } ?> value="T">T</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Pinjaman</Label>
							<select name="tmpl_pinjaman" class="form-control">
								<option <?php if($data[0]->tmpl_pinjaman == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->tmpl_pinjaman == "T"){ echo 'selected="selected"'; } ?> value="T">T</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Angsuran</Label>
							<select name="tmpl_bayar" class="form-control">
								<option <?php if($data[0]->tmpl_bayar == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->tmpl_bayar == "T"){ echo 'selected="selected"'; } ?> value="T">T</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Pemasukan Kas</Label>
							<select name="tmpl_pemasukan" class="form-control">
								<option <?php if($data[0]->tmpl_pemasukan == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->tmpl_pemasukan == "T"){ echo 'selected="selected"'; } ?> value="T">T</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Pengeluaran Kas</Label>
							<select name="tmpl_pengeluaran" class="form-control">
								<option <?php if($data[0]->tmpl_pengeluaran == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->tmpl_pengeluaran == "T"){ echo 'selected="selected"'; } ?> value="T">T</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Transfer Kas</Label>
							<select name="tmpl_transfer" class="form-control">
								<option <?php if($data[0]->tmpl_transfer == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->tmpl_transfer == "T"){ echo 'selected="selected"'; } ?> value="T">T</option>
							</select>
						</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('MasterData/data_kas');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
