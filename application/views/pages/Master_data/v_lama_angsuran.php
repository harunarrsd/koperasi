<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Jenis Angsuran
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Jenis Angsuran</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Jenis Angsuran</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="create_lama_angsuran" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah Jenis Angsuran</a>
				</div>
				<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped dataTable">
						<thead>
							<tr>
								<th>No.</th>
								<th>Lama Angsuran (Bulan)</th>
								<th>Aktif</th>
								<th>Pilihan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no;?></td>
								<td><?php echo $d->ket ?></td>
								<td><?php echo $d->aktif ?></td>
								<td>
									<a href="<?php echo site_url();?>MasterData/edit_lama_angsuran/<?php echo $d->id ?>" class="btn btn-success" title="Ubah Data"><i class="glyphicon glyphicon-edit"></i></a>
									<a href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>MasterData/delete_lama_angsuran/<?php echo $d->id ?>';}" class="btn btn-danger" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</section>
