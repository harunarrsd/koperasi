<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Jenis Akun Transaksi
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("MasterData/jns_akun")?>">Jenis Akun Transaksi</a></li>
      <li class="active">Buat Jenis Akun Transaksi</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'MasterData/update_jns_akun'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Kode Aktiva</Label>
							<input type="hidden" class="form-control" name="id" value="<?php echo $data[0]->id ?>">
							<input type="text" class="form-control" name="kd_aktiva" value="<?php echo $data[0]->kd_aktiva ?>">
						</div>
						<div class="form-group">
							<Label>Jenis Transaksi</Label>
							<input type="text" class="form-control" name="jns_trans" value="<?php echo $data[0]->jns_trans ?>">
						</div>
						<div class="form-group">
							<Label>Akun</Label>
							<select name="akun" class="form-control" placeholder="Akun">
								<option <?php if($data[0]->akun == "Pasiva"){ echo 'selected="selected"'; } ?> value="Pasiva">Pasiva</option>
								<option <?php if($data[0]->akun == "Aktiva"){ echo 'selected="selected"'; } ?> value="Aktiva">Aktiva</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Pemasukan</Label>
							<select name="pemasukan" class="form-control" placeholder="Akun">
								<option <?php if($data[0]->pemasukan == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->pemasukan == "N"){ echo 'selected="selected"'; } ?> value="N">N</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Pengeluaran</Label>
							<select name="pengeluaran" class="form-control" placeholder="Akun">
								<option <?php if($data[0]->pengeluaran == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->pengeluaran == "N"){ echo 'selected="selected"'; } ?> value="N">N</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Aktif</Label>
							<select name="aktif" class="form-control" placeholder="Akun">
								<option <?php if($data[0]->aktif == "Y"){ echo 'selected="selected"'; } ?> value="Y">Y</option>
								<option <?php if($data[0]->aktif == "N"){ echo 'selected="selected"'; } ?> value="N">N</option>
							</select>
							</select>
						</div>
						<div class="form-group">
							<Label>Laba Rugi</Label>
							<select name="laba_rugi" class="form-control" placeholder="Akun">
								<option <?php if($data[0]->laba_rugi == "PENDAPATAN"){ echo 'selected="selected"'; } ?> value="PENDAPATAN">Pendapatan</option>
								<option <?php if($data[0]->laba_rugi == "BIAYA"){ echo 'selected="selected"'; } ?> value="BIAYA">Biaya</option>
							</select>
						</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('MasterData/jns_akun');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
