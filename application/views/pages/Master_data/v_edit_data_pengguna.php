<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      User
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("MasterData/data_pengguna")?>">User</a></li>
      <li class="active">Edit User</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'MasterData/update_data_pengguna'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Username</Label>
							<input type="hidden" class="form-control" name="id" value="<?php echo $data[0]->id ?>">
							<input type="text" class="form-control" name="u_name" value="<?php echo $data[0]->u_name ?>">
						</div>
						<div class="form-group">
							<Label>Level</Label>
							<select name="level" class="form-control" placeholder="Level">
							<?php foreach ($data as $d) { ?>
								<option <?php if($d->id == $data[0]->id){ echo 'selected="selected"'; } ?> value="<?php echo $d->level ?>"><?php echo $d->level?> </option>
							<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<Label>Aktif</Label>
							<select name="aktif" class="form-control" placeholder="Aktif">
							<?php foreach ($data as $d) { ?>
								<option <?php if($d->id == $data[0]->id){ echo 'selected="selected"'; } ?> value="<?php echo $d->aktif ?>"><?php echo $d->aktif?> </option>
							<?php } ?>
							</select>
						</div>
						<div class="row">
							<div class="col-lg-4">
								<Label>Password Lama anda</Label>
								<input type="password" class="form-control" name="old_pass_word">
							</div>
							<div class="col-lg-4">
								<Label>Password Baru anda</Label>
								<input type="password" class="form-control" name="new_pass_word">
							</div>
							<div class="col-lg-4">
								<Label>Konfirmasi Password Baru anda</Label>
								<input type="password" class="form-control" name="confirm_pass_word">
							</div>
						</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('MasterData/data_pengguna');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
