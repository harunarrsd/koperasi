<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Data Barang
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("MasterData/data_barang")?>">Data Barang</a></li>
      <li class="active">Edit Data Barang</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'MasterData/update_data_barang'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Nama</Label>
							<input type="hidden" class="form-control" name="id" value="<?php echo $data[0]->id ?>">
							<input type="text" class="form-control" name="nm_barang" value="<?php echo $data[0]->nm_barang ?>">
						</div>
						<div class="form-group">
							<Label>Tipe</Label>
							<input type="text" class="form-control" name="type" value="<?php echo $data[0]->type ?>">
						</div>
						<div class="form-group">
							<Label>Merk</Label>
							<input type="text" class="form-control" name="merk" value="<?php echo $data[0]->merk ?>">
						</div>
						<div class="form-group">
							<Label>Harga</Label>
							<input type="text" class="form-control" name="harga" value="<?php echo $data[0]->harga ?>">
						</div>
						<div class="form-group">
							<Label>Jumlah Barang</Label>
							<input type="text" class="form-control" name="jml_brg" value="<?php echo $data[0]->jml_brg ?>">
						</div>
						<div class="form-group">
							<Label>Keterangan</Label>
							<input type="text" class="form-control" name="ket" value="<?php echo $data[0]->ket ?>">
						</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('MasterData/data_barang');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
