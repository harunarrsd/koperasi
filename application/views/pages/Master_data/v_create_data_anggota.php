<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Master Data
    </h1>
    <h5 class="inline text-muted">
      Data Anggota
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("MasterData/data_anggota")?>">Data Anggota</a></li>
      <li class="active">Buat Data Anggota</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'MasterData/create_data_anggota_action'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Nama Lengkap</Label>
							<input type="text" class="form-control" name="nama">
						</div>
						<div class="form-group">
							<Label>Username</Label>
							<input type="text" class="form-control" name="identitas">
						</div>
						<div class="form-group">
							<label>Jenis Kelamin</label>
							<div class="form-check form-check-inline">
								<input type="radio" class="form-check-input" name="jk" value="L">
								<label class="form-check-label">Laki-laki</label>
							</div>
							<div class="form-check form-check-inline">
								<input type="radio" class="form-check-input" name="jk" value="P">
								<label class="form-check-label">Perempuan</label>
							</div>
						</div>
						<div class="form-group">
							<Label>Alamat</Label>
							<textarea type="text" class="form-control" name="alamat"></textarea>
						</div>
						<div class="form-group">
							<Label>Kota</Label>
							<input type="text" class="form-control" name="kota">
						</div>
						<div class="form-group">
							<Label>Jabatan</Label>
							<select name="jabatan" class="form-control">
								<option value="Pengurus">Pengurus</option>
								<option value="Anggota">Anggota</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Departemen</Label>
							<select name="departement" class="form-control">
								<option value="Produksi BOPP">Produksi BOPP</option>
								<option value="Produksi Slitting">Produksi Slitting</option>
								<option value="WH">WH</option>
								<option value="QA">QA</option>
								<option value="HRD">HRD</option>
								<option value="GA">GA</option>
								<option value="Purchasing">Purchasing</option>
								<option value="Accounting">Accounting</option>
								<option value="Engineering">Engineering</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Aktif Keanggotaan</Label>
							<select name="aktif" class="form-control">
								<option value="Y">Aktif</option>
								<option value="N">Non Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Tempat Lahir</Label>
							<input type="text" class="form-control" name="tmp_lahir">
						</div>
						<div class="form-group">
							<Label>Tanggal Lahir</Label>
							<input type="date" class="form-control" name="tgl_lahir">
						</div>
						<div class="form-group">
							<Label>Status</Label>
							<select name="status" class="form-control">
								<option value="Belum Kawin">Belum Kawin</option>
								<option value="Kawin">Kawin</option>
								<option value="Cerai Hidup">Cerai Hidup</option>
								<option value="Cerai Mati">Cerai Mati</option>
								<option value="Lainnya">Lainnya</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Agama</Label>
							<select name="agama" class="form-control">
								<option value="Islam">Islam</option>
								<option value="Katolik">Katolik</option>
								<option value="Protestan">Protestan</option>
								<option value="Hindhu">Hindhu</option>
								<option value="Buddha">Buddha</option>
								<option value="Konghucu">Konghucu</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Pekerjaan</Label>
							<select name="pekerjaan" class="form-control">
								<?php foreach($data1 as $d){ ?>
									<option value="<?php echo $d->jenis_kerja ?>"><?php echo $d->jenis_kerja ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<Label>No Telepon</Label>
							<input type="text" class="form-control" name="notelp">
						</div>
						<div class="form-group">
							<Label>Password</Label>
							<input type="password" class="form-control" name="pass_word">
						</div>
						<div class="form-group">
							<Label>Foto Profil</Label>
							<input type="file" class="form-control-file" name="file_pic">
						</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('MasterData/data_anggota');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
