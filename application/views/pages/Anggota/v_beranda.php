<!-- Page Content -->
<!-- <section> -->
    <section class="container">
      <div class="row">
		  <div class="col-sm-12">
			  <div class="box box-primary">
				  <div class="box-body">
					  <div class="text-center">
						  <h3>Laporan Data Kas Anggota</h3>
					  </div>
					  <table class="table table-bordered table-reponsive">
						  <thead class="text-center bg-gray">
							  <th width="20%">Foto</th>
							  <th>Identitas</th>
						  </thead>
						  <tbody>
							  <tr>
								  <td rowspan="5"><?php echo $data[0]->file_pic ?></td>
								  <td>ID Anggota: <?php echo 'AG' . sprintf('%05d', $data[0]->id) ?></td>
							  </tr>
							  <tr>
								  <td>Nama: <strong><?php echo strtoupper($data[0]->nama) ?></strong></td>
							  </tr>
							  <tr>
								  <td>Jenis Kelamin: <?php if($data[0]->nama = 'L'){echo 'Laki-laki';} else{ echo 'Perempuan';}  ?></td>
							  </tr>
							  <tr>
								  <td>Jabatan: <?php echo $data[0]->jabatan  ?></td>
							  </tr>
							  <tr>
								  <td>Alamat: <?php echo $data[0]->alamat  ?></td>
							  </tr>
						  </tbody>
					  </table>
					  <br>
						<h3>Saldo Simpanan</h3>
						<table class="table table-responsive">
							<tr>
								<td>Simpanan Pokok</td>
								<td><?php echo number_format($data1[0]->jml_simpanan)  ?></td>
							</tr>
							<tr>
								<td>Simpanan Wajib</td>
								<td><?php echo number_format($data2[0]->jml_simpanan)  ?></td>
							</tr>
							<tr>
								<td>Simpanan Sukarelas</td>
								<td><?php echo number_format($data3[0]->jml_simpanan)  ?></td>
							</tr>
							<tr>
								<td><strong>Jumlah Simpanan</strong> </td>
								<td><strong><?php echo number_format($data1[0]->jml_simpanan + $data2[0]->jml_simpanan + $data3[0]->jml_simpanan) ?></strong></td>
							</tr>
						</table>
					  <br>
						<h3>Tagihan Kredit</h3>
						<table class="table table-responsive">
							<tr>
								<td>Pokok Pinjaman</td>
								<td><?php echo number_format($data4[0]->jml_pinjaman) ?></td>
							</tr>
							<tr>
								<td>Tagihan + Denda</td>
								<td><?php echo 'tes' ?></td>
							</tr>
							<tr>
								<td>Dibayar</td>
								<td><?php echo 'tes' ?></td>
							</tr>
							<tr>
								<td><strong>Sisa Tagihan</strong> </td>
								<td><strong><?php echo 'tes' ?></strong></td>
							</tr>
						</table>
					  <br>
						<h3>Keterangan</h3>
						<table class="table table-responsive">
							<tr>
								<td>Jumlah Pinjaman</td>
								<td><?php echo 'tes' ?></td>
							</tr>
							<tr>
								<td>Pinjaman Lunas</td>
								<td><?php echo 'tes' ?></td>
							</tr>
							<tr>
								<td>Pembayaran</td>
								<td><?php echo 'tes' ?></td>
							</tr>
							<tr>
								<td>Tanggal Tempo</td>
								<td><?php echo 'tes' ?></td>
							</tr>
						</table>
				  </div>
			  </div>
		  </div>
      </div>
    </div>
<!-- </section> -->
