<!-- Page Content -->
<!-- <section> -->
<section class="container">
      <div class="row">
		  <div class="col-sm-12">
			  <div class="box box-primary">
				  <div class="box-body">
					  <div class="text-center">
						  <h3>Data Pengajuan</h3>
						  <?php echo $this->session->flashdata('notif')?>
					  </div>
					  <table class="table table-bordered table-reponsive text-center">
						  <thead class="bg-gray">
							  <th>Tanggal</th>
							  <th>Jenis</th>
							  <th>Jumlah</th>
							  <th>Jml Angsur</th>
							  <th>Keterangan</th>
							  <th>Status</th>
							  <th>Alasan</th>
							  <th>Tanggal Update</th>
						  </thead>
						  <tbody>
							  <?php foreach($data1 as $d){ ?>
							  <tr>
								  <td><?php echo date("d M Y", strtotime($d->tgl_input)) ?></td>
								  <td><?php echo $d->jenis ?></td>
								  <td><?php echo number_format($d->nominal) ?></td>
								  <td><?php echo $d->lama_ags ?> bulan</td>
								  <td><?php echo $d->keterangan ?></td>
								  <td><?php if($d->status == 0) {
												echo '<span class="text-primary"><i class="fa fa-question"></i> Menunggu Konfirmasi';
											}
											if($d->status == 1) {
												echo '<span class="text-success"><i class="fa fa-check-circle"></i> Disetujui';
												echo '<br>Cair: ' . date("d M Y", strtotime($d->tgl_cair));
											}
											if($d->status == 2) {
												echo '<span class="text-danger"><i class="fa fa-times-circle"></i> Ditolak';
											}
											if($d->status == 3) {
												echo '<span class="text-success"><i class="fa fa-rocket"></i> Terlaksana';
											}
											if($d->status == 4) {
												echo '<span class="text-warning"><i class="fa fa-trash-o"></i> Batal';
											} ?></td>
								  <td><?php echo $d->alasan ?></td>
								  <td><?php echo date("d M Y", strtotime($d->tgl_update)) ?></td>
							  </tr>
							  <?php } ?>
						  </tbody>
					  </table>
				  </div>
			  </div>
		  </div>
      </div>
    </div>
<!-- </section> -->
