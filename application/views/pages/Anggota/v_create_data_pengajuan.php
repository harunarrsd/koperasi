  <!-- Main content -->
<section class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header">
					<h3>Formulir Pengajuan</h3>
				</div>
				<!-- Form -->
				<form action="<?php echo base_url(). 'Anggota/create_data_pengajuan_action'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Jenis</Label>
							<select name="jenis" class="form-control">
								<option value="Biasa">Biasa</option>
								<option value="Barang">Barang</option>
								<option value="Darurat">Darurat</option>
							</select>
						</div>
						<div class="form-group">
							<Label>Nominal</Label>
							<input type="text" class="form-control" name="nominal">
						</div>
						<div class="form-group">
							<Label>Lama Angsuran</Label>
							<select name="lama_angsuran" class="form-control">
								<?php foreach($data3 as $d){ ?>
								<option value="<?php echo $d->ket ?>"><?php echo $d->ket ?></option>';
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<Label>Keterangan</Label>
							<textarea class="form-control" name="keterangan"></textarea>
						</div>
					</div>
					<div class="box-footer">
						<a href="<?php echo site_url('MasterData/data_pengguna');?>" class="btn btn-danger">Kembali</a>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
