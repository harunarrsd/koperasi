<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Transaksi
    </h1>
    <h5 class="inline text-muted">
	  Setoran Tunai
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="<?php echo site_url("Simpanan/setoran_tunai")?>"> Setoran Tunai</a></li>
      <li class="active">Edit Data Setoran Tunai</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<!-- Form -->
				<form action="<?php echo base_url(). 'Simpanan/update_setoran_tunai'; ?>" method="post">
					<div class="box-body">
							<div class="form-group">
								<Label>Tanggal Transaksi</Label>
								<input type="hidden" class="form-control" name="id" value="<?php echo $data[0]->id ?>">
								<input type="text" class="form-control" name="tgl_transaksi" id="datepicker" value="<?php echo $data[0]->tgl_transaksi ?>">
							</div>
							<div class="form-group">
								<h4><strong>Identitas Penyetor</strong></h4>
							</div>
								<div class="form-group">
									<Label>Nama Penyetor</Label>
									<input type="text" class="form-control" name="nama_penyetor" value="<?php echo $data[0]->nama_penyetor ?>">
								</div>
								<div class="form-group">
									<Label>Nomor Identitas</Label>
									<input type="text" class="form-control" name="no_identitas" value="<?php echo $data[0]->no_identitas ?>">
								</div>
								<div class="form-group">
									<Label>Alamat</Label>
									<textarea class="form-control" name="alamat"><?php echo $data[0]->alamat ?></textarea>
								</div>
							<div class="form-group">
								<h4><strong>Identitas Penerima</strong></h4>
							</div>
								<div class="form-group">
									<Label>Jenis Simpanan</Label>
									<select name="jenis_id" class="form-control">
										<?php foreach($data2 as $d){ ?>
											<option value="<?php echo $d->id ?>" <?php if($d->id == $data[0]->jenis_id){ echo 'selected="selected"';} ?>><?php echo $d->jns_simpan ?></option>';
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<Label>Jumlah Simpanan</Label>
									<input type="text" class="form-control" name="jumlah" value="<?php echo $data[0]->jumlah ?>">
								</div>
								<div class="form-group">
									<Label>Keterangan</Label>
									<textarea class="form-control" name="keterangan"><?php echo $data[0]->keterangan ?></textarea>
								</div>
								<div class="form-group">
									<Label>Simpan ke Kas</Label>
									<select name="kas_id" class="form-control">
									<?php foreach($data3 as $d){ ?>
										<option value="<?php echo $d->id ?>" <?php if($d->id == $data[0]->kas_id){ echo 'selected="selected"';} ?>><?php echo $d->nama ?></option>';
									<?php } ?>
									</select>
								</div>
						</div>
						<div class="box-footer">
							<a href="<?php echo site_url('Simpanan/setoran_tunai');?>" class="btn btn-danger">Kembali</a>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
				</form>
			</div>
		</div>
	</div>
</section>
