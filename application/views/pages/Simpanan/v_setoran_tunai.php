<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Transaksi
    </h1>
    <h5 class="inline text-muted">
      Setoran Tunai
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> Setoran Tunai</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<!-- Tabel Jenis Simpanan -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Data Transaksi Setoran Tunai</h4>
					<?php echo $this->session->flashdata('notif')?>
					<a href="create_setoran_tunai" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
				</div>
				<div class="box-body table-responsive">
					<table class="table table-bordered table-striped dataTable">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode Transaksi</th>
								<th>Tanggal Transaksi</th>
								<th>ID Anggota</th>
								<th>Nama Anggota</th>
								<th>Dept</th>
								<th>Jenis Simpanan</th>
								<th>Jumlah</th>
								<th>User</th>
								<th>Pilihan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=0; 
							foreach($data as $d){ 
							$no++; ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo 'TRD' . sprintf('%05d', $d->id_simpan) . '' ?></td>
								<td><?php echo date("d-m-Y", strtotime($d->tgl)) ?></td>
								<td><?php echo $d->id_anggota ?></td>
								<td><?php echo $d->nama_anggota ?></td>
								<td><?php echo $d->departement ?></td>
								<td><?php echo $d->jns_simpan ?></td>
								<td><?php echo $d->jml ?></td>
								<td><?php echo $d->user ?></td>
								<td>
									<a href="edit_setoran_tunai/<?php echo $d->id_simpan ?>" class="btn btn-success" title="Ubah Data"><i class="glyphicon glyphicon-edit"></i></a>
									<a href="javascript:if(confirm('Apakah Anda yakin ?')){document.location='<?php echo site_url();?>Simpanan/delete_setoran_tunai/<?php echo $d->id_simpan ?>';}" class="btn btn-danger" title="Hapus Data"><i class="glyphicon glyphicon-trash"></i></a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</section>
