<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Setting
    </h1>
    <h5 class="inline text-muted">
      Profil
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Identitas Koperasi</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Update Data Koperasi</h4>
					<?php echo $this->session->flashdata('notif')?>
				</div>
				<!-- Form -->
				<form action="<?php echo base_url(). 'Setting/update_identitas_koperasi'; ?>" method="post">
					<div class="box-body">
						<div class="form-group">
							<Label>Nama Koperasi</Label>
							<input type="hidden" class="form-control" name="id_nama_lembaga" value="<?php echo $data[0]->id ?>">
							<input type="text" class="form-control" name="nama_lembaga" value="<?php echo $data[0]->opsi_val ?>">
						</div>
						<div class="form-group">
							<Label>Nama Pimpinan</Label>
							<input type="hidden" class="form-control" name="id_nama_ketua" value="<?php echo $data[1]->id ?>">
							<input type="text" class="form-control" name="nama_ketua" value="<?php echo $data[1]->opsi_val ?>">
						</div>
						<div class="form-group">
							<Label>No. HP</Label>
							<input type="hidden" class="form-control" name="id_hp_ketua" value="<?php echo $data[2]->id ?>">
							<input type="text" class="form-control" name="hp_ketua" value="<?php echo $data[2]->opsi_val ?>">
						</div>
						<div class="form-group">
							<Label>Alamat</Label>
							<input type="hidden" class="form-control" name="id_alamat" value="<?php echo $data[3]->id ?>">
							<textarea type="text" class="form-control" name="alamat"><?php echo $data[3]->opsi_val ?></textarea>
						</div>
						<div class="form-group">
							<Label>Telepon</Label>
							<input type="hidden" class="form-control" name="id_telepon" value="<?php echo $data[4]->id ?>">
							<input type="text" class="form-control" name="telepon" value="<?php echo $data[4]->opsi_val ?>">
						</div>
						<div class="form-group">
							<Label>Kota/Kabupaten</Label>
							<input type="hidden" class="form-control" name="id_kota" value="<?php echo $data[5]->id ?>">
							<input type="text" class="form-control" name="kota" value="<?php echo $data[5]->opsi_val ?>">
						</div>
						<div class="form-group">
							<Label>Email</Label>
							<input type="hidden" class="form-control" name="id_email" value="<?php echo $data[6]->id ?>">
							<input type="email" class="form-control" name="email" value="<?php echo $data[6]->opsi_val ?>">
						</div>
						<div class="form-group">
							<Label>Website</Label>
							<input type="hidden" class="form-control" name="id_web" value="<?php echo $data[7]->id ?>">
							<input type="text" class="form-control" name="web" value="<?php echo $data[7]->opsi_val ?>">
						</div>

					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
