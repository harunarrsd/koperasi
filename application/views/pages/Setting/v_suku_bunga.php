<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="inline">
      Setting
    </h1>
    <h5 class="inline text-muted">
      Biaya dan Administrasi
    </h5>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url("home")?>"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Suku Bunga</li>
    </ol>
</section>

  <!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header">
					<h4>Biaya dan Administrasi</h4>
					<?php echo $this->session->flashdata('notif')?>
				</div>
				<!-- Form -->
				<form action="<?php echo base_url(). 'Setting/update_suku_bunga'; ?>" method="post">
					<div class="box-body">
						<div class="form-row">
							<div class="form-group col-md-4">
								<Label>Suku Bunga Tabungan (%)</Label>
								<input type="hidden" class="form-control" name="id_bg_tab" value="<?php echo $data[0]->id ?>">
								<input type="text" class="form-control" name="bg_tab" value="<?php echo $data[0]->opsi_val ?>">
							</div>
							<div class="form-group col-md-4">
								<Label>Suku Bunga Pinjaman (%)</Label>
								<input type="hidden" class="form-control" name="id_bg_pinjam" value="<?php echo $data[1]->id ?>">
								<input type="text" class="form-control" name="bg_pinjam" value="<?php echo $data[1]->opsi_val ?>">
							</div>
							<div class="form-group col-md-4">
								<Label>Biaya Administrasi (Rp)</Label>
								<input type="hidden" class="form-control" name="id_biaya_adm" value="<?php echo $data[2]->id ?>">
								<input type="text" class="form-control" name="biaya_adm" value="<?php echo $data[2]->opsi_val ?>">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<Label>Biaya Denda (Rp)</Label>
								<input type="hidden" class="form-control" name="id_denda" value="<?php echo $data[3]->id ?>">
								<input type="text" class="form-control" name="denda" value="<?php echo $data[3]->opsi_val ?>">
							</div>
							<div class="form-group col-md-4">
								<Label>Tempo Tanggal Pembayaran</Label>
								<input type="hidden" class="form-control" name="id_denda_hari" value="<?php echo $data[4]->id ?>">
								<input type="text" class="form-control" name="denda_hari" value="<?php echo $data[4]->opsi_val ?>">
							</div>
							<div class="form-group col-md-4">
								<Label>Dana Cadangan (%)</Label>
								<input type="hidden" class="form-control" name="id_dana_cadangan" value="<?php echo $data[5]->id ?>">
								<input type="text" class="form-control" name="dana_cadangan" value="<?php echo $data[5]->opsi_val ?>">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<Label>Jasa Anggota (%)</Label>
								<input type="hidden" class="form-control" name="id_jasa_anggota" value="<?php echo $data[6]->id ?>">
								<input type="text" class="form-control" name="jasa_anggota" value="<?php echo $data[6]->opsi_val ?>">
							</div>
							<div class="form-group col-md-4">
								<Label>Dana Pengurus (%)</Label>
								<input type="hidden" class="form-control" name="id_dana_pengurus" value="<?php echo $data[7]->id ?>">
								<input type="text" class="form-control" name="dana_pengurus" value="<?php echo $data[7]->opsi_val ?>">
							</div>
							<div class="form-group col-md-4">
								<Label>Dana Karyawan (%)</Label>
								<input type="hidden" class="form-control" name="id_dana_karyawan" value="<?php echo $data[8]->id ?>">
								<input type="text" class="form-control" name="dana_karyawan" value="<?php echo $data[8]->opsi_val ?>">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<Label>Dana Pendidikan (%)</Label>
								<input type="hidden" class="form-control" name="id_dana_pend" value="<?php echo $data[9]->id ?>">
								<input type="text" class="form-control" name="dana_pend" value="<?php echo $data[9]->opsi_val ?>">
							</div>
							<div class="form-group col-md-4">
								<Label>Dana Sosial (%)</Label>
								<input type="hidden" class="form-control" name="id_dana_sosial" value="<?php echo $data[10]->id ?>">
								<input type="text" class="form-control" name="dana_sosial" value="<?php echo $data[10]->opsi_val ?>">
							</div>
							<div class="form-group col-md-4">
								<Label>Jasa Usaha (%)</Label>
								<input type="hidden" class="form-control" name="id_jasa_usaha" value="<?php echo $data[11]->id ?>">
								<input type="text" class="form-control" name="jasa_usaha" value="<?php echo $data[11]->opsi_val ?>">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<Label>Jasa Modal Anggota (%)</Label>
								<input type="hidden" class="form-control" name="id_jasa_modal" value="<?php echo $data[12]->id ?>">
								<input type="text" class="form-control" name="jasa_modal" value="<?php echo $data[12]->opsi_val ?>">
							</div>
							<div class="form-group col-md-4">
								<Label>Pajak PPh (%)</Label>
								<input type="hidden" class="form-control" name="id_pjk_pph" value="<?php echo $data[13]->id ?>">
								<input type="text" class="form-control" name="pjk_pph" value="<?php echo $data[13]->opsi_val ?>">
							</div>
							<div class="form-group col-md-4">
								<Label>Tipe Pinjaman Bunga </Label>
								<input type="hidden" class="form-control" name="id_pinjaman_bunga_tipe" value="<?php echo $data[14]->id ?>">
								<select name="pinjaman_bunga_tipe" class="form-control">
									<option <?php if($data[14]->opsi_val == "A"){ echo 'selected="selected"'; } ?> value="A">A: Persen Bunga dikali angsuran bln</option>
									<option <?php if($data[14]->opsi_val == "B"){ echo 'selected="selected"'; } ?> value="B">B: Persen Bunga dikali total pinjaman</option>
								</select>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
