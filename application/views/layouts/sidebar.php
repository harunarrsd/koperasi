<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <!-- <li class="header">MAIN NAVIGATION</li> -->
        <li class="<?php if($this->uri->segment(1) == 'home') { echo 'active open'; } ?>">
          <a href="<?php echo site_url("home")?>">
            <i class="fa fa-dashboard"></i>
            <span>Beranda</span>
          </a>
				</li>
				<?php
				if($this->session->userdata('level')=='admin'){
				?>
					<li class="treeview <?php if($this->uri->segment(1) == 'TransaksiKas') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Transaksi Kas</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'TransaksiKas/transaksi_kas_pemasukan'?>"><i class="fa fa-circle"></i> Transaksi Kas Pemasukan</a></li>
							<li ><a href="<?php echo base_url().'TransaksiKas/transaksi_kas_pengeluaran'?>"><i class="fa fa-circle"></i> Transaksi Kas Pengeluaran</a></li>
							<li ><a href="<?php echo base_url().'TransaksiKas/transaksi_kas_transfer'?>"><i class="fa fa-circle"></i> Transaksi Kas Transfer</a></li>
						</ul>
					</li>
					<li class="treeview <?php if($this->uri->segment(1) == 'Simpanan') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Simpanan</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'Simpanan/setoran_tunai'?>"><i class="fa fa-circle"></i> Setoran Tunai</a></li>
							<li ><a href="<?php echo base_url().'Simpanan/penarikan_tunai'?>"><i class="fa fa-circle"></i> Penarikan Tunai</a></li>
						</ul>
					</li>
					<li class="treeview <?php if($this->uri->segment(1) == 'Pinjaman') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Pinjaman</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'Pinjaman/data_pengajuan'?>"><i class="fa fa-circle"></i> Data Pengajuan</a></li>
							<li><a href="<?php echo base_url().'Pinjaman/data_pinjaman'?>"><i class="fa fa-circle"></i> Data Pinjaman</a></li>
							<li><a href="<?php echo base_url().'Pinjaman/bayar_angsuran'?>"><i class="fa fa-circle"></i> Bayar Angsuran</a></li>
							<li><a href="<?php echo base_url().'Pinjaman/pelunasan_pinjaman'?>"><i class="fa fa-circle"></i> Pinjaman Lunas</a></li>
						</ul>
					</li>
					<li class="treeview <?php if($this->uri->segment(1) == 'Laporan') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Laporan</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'Laporan/data_anggota'?>"><i class="fa fa-circle"></i> Data Anggota</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kas_anggota'?>"><i class="fa fa-circle"></i> Kas Anggota</a></li>
							<li ><a href="<?php echo base_url().'Laporan/jatuh_tempo'?>"><i class="fa fa-circle"></i> Jatuh Tempo</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kredit_macet'?>"><i class="fa fa-circle"></i> Kredit Macet</a></li>
							<li ><a href="<?php echo base_url().'Laporan/transaksi_kas'?>"><i class="fa fa-circle"></i> Transaksi Kas</a></li>
							<li ><a href="<?php echo base_url().'Laporan/buku_besar'?>"><i class="fa fa-circle"></i> Buku Besar</a></li>
							<li ><a href="<?php echo base_url().'Laporan/neraca_saldo'?>"><i class="fa fa-circle"></i> Neraca Saldo</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kas_simpanan'?>"><i class="fa fa-circle"></i> Kas Simpanan</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kas_pinjaman'?>"><i class="fa fa-circle"></i> Kas Pinjaman</a></li>
							<li ><a href="<?php echo base_url().'Laporan/saldo_kas'?>"><i class="fa fa-circle"></i> Saldo Kas</a></li>
							<li ><a href="<?php echo base_url().'Laporan/laba_rugi'?>"><i class="fa fa-circle"></i> Laba Rugi</a></li>
						</ul>
					</li>
					<li class="treeview <?php if($this->uri->segment(1) == 'MasterData') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Master Data</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'MasterData/jns_simpanan' ?>"><i class="fa fa-circle"></i> Jenis Simpanan</a></li>
							<li ><a href="<?php echo base_url().'MasterData/jns_akun'?>"><i class="fa fa-circle"></i> Jenis Akun</a></li>
							<li ><a href="<?php echo base_url().'MasterData/data_kas'?>"><i class="fa fa-circle"></i> Data Kas</a></li>
							<li ><a href="<?php echo base_url().'MasterData/lama_angsuran'?>"><i class="fa fa-circle"></i> Lama Angsuran</a></li>
							<li ><a href="<?php echo base_url().'MasterData/data_barang'?>"><i class="fa fa-circle"></i> Data Barang</a></li>
							<li ><a href="<?php echo base_url().'MasterData/data_anggota'?>"><i class="fa fa-circle"></i> Data Anggota</a></li>
							<li ><a href="<?php echo base_url().'MasterData/data_pengguna'?>"><i class="fa fa-circle"></i> Data Pengguna</a></li>
						</ul>
					</li>
					<li class="treeview <?php if($this->uri->segment(1) == 'Setting') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Setting</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'Setting/identitas_koperasi'?>"><i class="fa fa-circle"></i> Identitas Koperasi</a></li>
							<li ><a href="<?php echo base_url().'Setting/suku_bunga'?>"><i class="fa fa-circle"></i> Suku Bunga</a></li>
						</ul>
					</li>
				<?php
				} else if($this->session->userdata('level')=='operator'){
				?>
					<li class="treeview <?php if($this->uri->segment(1) == 'TransaksiKas') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Transaksi Kas</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
						<li><a href="<?php echo base_url().'TransaksiKas/transaksi_kas_pemasukan'?>"><i class="fa fa-circle"></i> Transaksi Kas Pemasukan</a></li>
							<li ><a href="<?php echo base_url().'TransaksiKas/transaksi_kas_pengeluaran'?>"><i class="fa fa-circle"></i> Transaksi Kas Pengeluaran</a></li>
							<li ><a href="<?php echo base_url().'TransaksiKas/transaksi_kas_transfer'?>"><i class="fa fa-circle"></i> Transaksi Kas Transfer</a></li>
						</ul>
					</li>
					<li class="treeview <?php if($this->uri->segment(1) == 'Simpanan') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Simpanan</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'Simpanan/setoran_tunai'?>"><i class="fa fa-circle"></i> Setoran Tunai</a></li>
							<li ><a href="<?php echo base_url().'Simpanan/penarikan_tunai'?>"><i class="fa fa-circle"></i> Penarikan Tunai</a></li>
						</ul>
					</li>
					<li class="treeview <?php if($this->uri->segment(1) == 'Pinjaman') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Pinjaman</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'Pinjaman/data_pengajuan'?>"><i class="fa fa-circle"></i> Data Pengajuan</a></li>
							<li><a href="<?php echo base_url().'Pinjaman/data_pinjaman'?>"><i class="fa fa-circle"></i> Data Pinjaman</a></li>
							<li><a href="<?php echo base_url().'Pinjaman/bayar_angsuran'?>"><i class="fa fa-circle"></i> Bayar Angsuran</a></li>
							<li><a href="<?php echo base_url().'Pinjaman/pelunasan_pinjaman'?>"><i class="fa fa-circle"></i> Pinjaman Lunas</a></li>
						</ul>
					</li>
					<li class="treeview <?php if($this->uri->segment(1) == 'Laporan') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Laporan</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'Laporan/data_anggota'?>"><i class="fa fa-circle"></i> Data Anggota</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kas_anggota'?>"><i class="fa fa-circle"></i> Kas Anggota</a></li>
							<li ><a href="<?php echo base_url().'Laporan/jatuh_tempo'?>"><i class="fa fa-circle"></i> Jatuh Tempo</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kredit_macet'?>"><i class="fa fa-circle"></i> Kredit Macet</a></li>
							<li ><a href="<?php echo base_url().'Laporan/transaksi_kas'?>"><i class="fa fa-circle"></i> Transaksi Kas</a></li>
							<li ><a href="<?php echo base_url().'Laporan/buku_besar'?>"><i class="fa fa-circle"></i> Buku Besar</a></li>
							<li ><a href="<?php echo base_url().'Laporan/neraca_saldo'?>"><i class="fa fa-circle"></i> Neraca Saldo</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kas_simpanan'?>"><i class="fa fa-circle"></i> Kas Simpanan</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kas_pinjaman'?>"><i class="fa fa-circle"></i> Kas Pinjaman</a></li>
							<li ><a href="<?php echo base_url().'Laporan/saldo_kas'?>"><i class="fa fa-circle"></i> Saldo Kas</a></li>
							<li ><a href="<?php echo base_url().'Laporan/laba_rugi'?>"><i class="fa fa-circle"></i> Laba Rugi</a></li>
						</ul>
					</li>
					<li class="treeview <?php if($this->uri->segment(1) == 'MasterData') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Master Data</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'MasterData/jns_simpanan' ?>"><i class="fa fa-circle"></i> Jenis Simpanan</a></li>
							<li ><a href="<?php echo base_url().'MasterData/jns_akun'?>"><i class="fa fa-circle"></i> Jenis Akun</a></li>
							<li ><a href="<?php echo base_url().'MasterData/data_kas'?>"><i class="fa fa-circle"></i> Data Kas</a></li>
							<li ><a href="<?php echo base_url().'MasterData/lama_angsuran'?>"><i class="fa fa-circle"></i> Lama Angsuran</a></li>
							<li ><a href="<?php echo base_url().'MasterData/data_barang'?>"><i class="fa fa-circle"></i> Data Barang</a></li>
							<li ><a href="<?php echo base_url().'MasterData/data_anggota'?>"><i class="fa fa-circle"></i> Data Anggota</a></li>
							<li ><a href="<?php echo base_url().'MasterData/data_pengguna'?>"><i class="fa fa-circle"></i> Data Pengguna</a></li>
						</ul>
					</li>
				<?php
				} else if($this->session->userdata('level')=='pinjaman'){
				?>
					<li class="treeview <?php if($this->uri->segment(1) == 'Pinjaman') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Pinjaman</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'Pinjaman/data_pengajuan'?>"><i class="fa fa-circle"></i> Data Pengajuan</a></li>
							<li><a href="<?php echo base_url().'Pinjaman/data_pinjaman'?>"><i class="fa fa-circle"></i> Data Pinjaman</a></li>
							<li><a href="<?php echo base_url().'Pinjaman/bayar_angsuran'?>"><i class="fa fa-circle"></i> Bayar Angsuran</a></li>
							<li><a href="<?php echo base_url().'Pinjaman/pelunasan_pinjaman'?>"><i class="fa fa-circle"></i> Pinjaman Lunas</a></li>
						</ul>
					</li>
					<li class="treeview <?php if($this->uri->segment(1) == 'Laporan') { echo 'active open'; } ?>">
						<a href="#">
							<i class="fa fa-user"></i>
							<span>Laporan</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo base_url().'Laporan/data_anggota'?>"><i class="fa fa-circle"></i> Data Anggota</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kas_anggota'?>"><i class="fa fa-circle"></i> Kas Anggota</a></li>
							<li ><a href="<?php echo base_url().'Laporan/jatuh_tempo'?>"><i class="fa fa-circle"></i> Jatuh Tempo</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kredit_macet'?>"><i class="fa fa-circle"></i> Kredit Macet</a></li>
							<li ><a href="<?php echo base_url().'Laporan/transaksi_kas'?>"><i class="fa fa-circle"></i> Transaksi Kas</a></li>
							<li ><a href="<?php echo base_url().'Laporan/buku_besar'?>"><i class="fa fa-circle"></i> Buku Besar</a></li>
							<li ><a href="<?php echo base_url().'Laporan/neraca_saldo'?>"><i class="fa fa-circle"></i> Neraca Saldo</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kas_simpanan'?>"><i class="fa fa-circle"></i> Kas Simpanan</a></li>
							<li ><a href="<?php echo base_url().'Laporan/kas_pinjaman'?>"><i class="fa fa-circle"></i> Kas Pinjaman</a></li>
							<li ><a href="<?php echo base_url().'Laporan/saldo_kas'?>"><i class="fa fa-circle"></i> Saldo Kas</a></li>
							<li ><a href="<?php echo base_url().'Laporan/laba_rugi'?>"><i class="fa fa-circle"></i> Laba Rugi</a></li>
						</ul>
					</li>
				<?php
				}
				?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
