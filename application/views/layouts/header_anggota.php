  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="<?php echo site_url("Anggota")?>">Anggota</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
      <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav">
          <li class="nav-item <?php if($this->uri->segment(1) == 'Anggota') { echo 'active'; } ?>">
            <a class="nav-link" href="<?php echo site_url("Anggota")?>">Beranda<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Pengajuan Pinjaman
			</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="<?php echo site_url("Anggota/data_pengajuan")?>">Data Pengajuan</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="<?php echo site_url("Anggota/create_data_pengajuan")?>">Tambah Pengajuan Baru</a>
			</div>
          </li>
          <li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Laporan
			</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="<?php echo site_url("Anggota/laporan_simpanan")?>">Simpanan</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="<?php echo site_url("Anggota/laporan_pinjaman")?>">Pinjaman</a>
				<a class="dropdown-item" href="<?php echo site_url("Anggota/laporan_pembayaran")?>">Pembayaran</a>
			</div>
          </li>
          <li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Profil
			</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="#">Ubah Gambar Profil</a>
				<a class="dropdown-item" href="#">Ubah Password</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="<?php echo site_url('main/keluar')?>">Logout</a>
			</div>
          </li>
		</ul>
      </div>
    </div>
  </nav>
